#!/usr/bin/python
import httplib, urllib
import os
from my_parser import parser

class Download_dict:
	
	def __init__(self, in_path, out_path):
		self.in_path = in_path
		self.out_path = out_path
		self.proteins = {}

	#This function reads the data from the file given to it. it saves the proteins name 
	#as a key in a dictionary and their sequences will be the value of the protein
	def get_yeast_p_name(self):
		try:
			in_f = open(self.in_path, 'r')
			for line in in_f:
				names = line.split(' ')
				names = filter(lambda a: a!= '', names)
				self.proteins[names[0]] = None
				self.proteins[names[1]] = None
		except IOError, e:
			print 'Some error while openning file happend', e
			
	
	def download_yeast_p_seq_dict(self):
		try:
			out_f = open(self.out_path, 'w')
			current = 0 
			total = len(self.proteins)
			for p in self.proteins:
				print 'For protein: ', p
				params = urllib.urlencode({'query': p, 'format' : 'Fasta', 'seqtype' : 'ORF Translation', 'submit' : 'Get Sequence'})
				url = urllib.urlopen("http://www.yeastgenome.org/cgi-bin/getSeq",params)
				parser.feed(url.read())
				url.close()
				seq =  parser.sequence
				parser.close()
				seq = seq[seq.find('\n'):]
				seq = filter(lambda a: a!='\n', seq)
				out_f.write(p + '\t' + seq +'\r\n')
				current = current + 1
				percent = current * 100.0 / total
				print seq, str(current) + ' out of ' + str(total) + '(' +  str(round(percent,2)) +'%)'
			other_file_format.close()
				

		except IOError, e:
			print 'Some error while openning file happend', e

#End of class download_dict

in_path = os.path.join(os.path.abspath('.'),'yeastN_random1.txt')
out_path = os.path.join(os.path.abspath('.'),'p_seq_dict_YN_formatted.txt')

get_dict = Download_dict(in_path,out_path)
get_dict.get_yeast_p_name()
get_dict.download_yeast_p_seq_dict()
