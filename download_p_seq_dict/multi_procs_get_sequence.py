#!/usr/bin/python
import httplib, urllib
import os, time
#from my_parser import parser
from my_parser import myHTMLParser
from multiprocessing import Manager, Process, managers

class Download_dict:
	
	def __init__(self, in_path, out_path, **kwargs):
		self.in_path = in_path
		self.out_path = out_path
		self.proteins = {}
		manager = Manager()
		self.proteins_q = manager.Queue()
		##### Following is used for the Secondary structure
		in_dict_path = kwargs.get('in_dict_path', None)
		self.p_to_seq_dict = manager.Queue()
		if in_dict_path:
			self.create_p_to_seq_dict_from_f(in_dict_path)
			

	# This function reads the data from the file given to it. it saves the 
	# proteins name as a key in a dictionary and their sequences will be the 
	# value of the protein
	def get_yeast_p_name(self):
		try:
			in_f = open(self.in_path, 'r')
			for line in in_f:
				names = line.split(' ')
				names = filter(lambda a: a!= '', names)
				self.proteins[names[0]] = None
				self.proteins[names[1]] = None
		except IOError, e:
			print 'Some error while openning file happend', e
		max = 150
		i = 0
		for p in self.proteins:
			self.proteins_q.put(p)
			i = i + 1
			if (i >=  50):
				pass
				#break
			
	# This function reads the data from the file given to it. it saves the 
	# proteins name as a key in a dictionary and their sequences will be the
	# value of the protein
	def get_human_p_name(self):
		try:
			in_f = open(self.in_path, 'r')
			for line in in_f:
				names = line.split(' ')
				names = filter(lambda a: a!= '', names)
				self.proteins[names[0]] = None
				self.proteins[names[1]] = None
		except IOError, e:
			print 'Some error while openning file happend', e
		max = 150
		i = 0
		for p in self.proteins:
			self.proteins_q.put(p)
			i = i + 1
			if (i >=  50):
				pass
				#break

	##########################################################################
	# Get data from website
	# Returns a list with data as first element and number of connections 
	# as second
	##########################################################################
	def __get_data_from_web__(self, url, params):
		num_of_tries = 1
		not_success = True
		while not_success:
			try:
				connection = urllib.urlopen(url,params)
				not_success = False
			except IOError,e:
				print p,' could not connect Will try again',e
				time.sleep(0.5)
				num_of_tries += 1

		data = connection.read()
		connection.close()

		return (data, num_of_tries)

	
	def download_yeast_p_seq_dict(self, out_f_shared_obj):
		try:
			#out_f = open(self.out_path, 'w')
			current = 0 
			out_f_shared_obj.set_total_num (len(self.proteins))
			#for p in self.proteins:
			while not self.proteins_q.empty():
				p = self.proteins_q.get()
				params = urllib.urlencode({
						'query': p,
						'format' : 'Fasta',
						'seqtype' : 'ORF Translation',
						'submit' : 'Get Sequence'
					})
				url = "http://www.yeastgenome.org/cgi-bin/getSeq" 

				data_num_tries_ls = self.__get_data_from_web__(url, params)
				data = data_num_tries_ls[0]
				num_of_tries = data_num_tries_ls[1]
				parser = myHTMLParser()
				parser.feed(data)

				if parser.sequence != None:
					seq =  parser.sequence
				else:
					seq = 'NotFound'
				parser.close()
				if seq.find('\n') != -1:
					seq = seq[seq.find('\n'):]
				seq = filter(lambda a: a!='\n', seq)
				seq = filter(lambda a: a!='*', seq)

				#out_f.write(p + '\t' + seq +'\r\n')
				#########
				print out_f_shared_obj.write_to_file(p, seq, num_of_tries,
											  True)
				#current = out_f_shared_obj.get_cur_p_num()
				#########
			#out_f_shared_obj.close_file()
				

		except IOError, e:
			print 'Some error while openning file happend', e


	def download_human_p_seq_dict(self, out_f_shared_obj):
		try:
			#out_f = open(self.out_path, 'w')
			current = 0 
			out_f_shared_obj.set_total_num (len(self.proteins))
			#for p in self.proteins:
			while not self.proteins_q.empty():
				p = self.proteins_q.get()
				params = None
				url = ("http://www.ebi.ac.uk/interpro/ISpy?ac=" + 
					  p.split('|')[1]+"&mode=fasta&sort=ac&width=880")

				data_num_tries_ls = self.__get_data_from_web__(url,params)

				num_of_tries = data_num_tries_ls[1]
				# The website just gives us string of data not an html file
				# for humans
				data = data_num_tries_ls[0]
				seq = data
				seq = seq[seq.find('\n'):]
				seq = filter(lambda a: a!='\n', seq)
				# This website contains some carriage return characters 
				# which need to be removed
				seq = filter(lambda a: a!='\r', seq)
				#out_f.write(p + '\t' + seq +'\r\n')
				#########
				print out_f_shared_obj.write_to_file(p, seq, num_of_tries,
											  True)
				#current = out_f_shared_obj.get_cur_p_num()
				#########
			#out_f_shared_obj.close_file()
				

		except IOError, e:
			print 'Some error while openning file happend', e
	

	######################################################	
	#Creat proteins to sequences dictionary from a file
	######################################################
	def create_p_to_seq_dict_from_f(self, in_path, sep='\t'):
		try:
			print 'Creating protein to sequence dictionary'
			print in_path
			f_in = open(in_path, 'r')
			for line in f_in:
				all = line.split(sep)
				p = all[0]
				seq = all[1]
				self.p_to_seq_dict.put( {p:seq} )
				self.proteins[p] = seq
			print 'Dictionary is made'
			f_in.close()
		except IOError,e:
			print 'cannot open the file',e

	
	
	def get_name_for_p_form_uniprot(self, p_name, p_seq):
		#Removing extra return characters at the end of each sequence
		p_seq = filter(lambda a: a!='\n', p_seq)
		# This website contains some carriage return characters which need to
		# be removed
		p_seq = filter(lambda a: a!='\r', p_seq)

		url = "http://www.uniprot.org/uniprot/?query="+p_name
		params = None
		
		
		data_num_tries_ls = self.__get_data_from_web__(url, params)

		data = data_num_tries_ls[0]
		parser = myHTMLParser()
		parser.feed(data)
		for row in parser.search_results:
			if  (row != [] and row[-1].find(',') == -1 and
				int(row[-1]) == len(p_seq)):
				return row[0]

		return p_name


	def __convert_uniprot_format_to_l__(self, uniprot_arr,length):
		out_l = "_" * length
		for (counter, row) in enumerate(uniprot_arr):
			start_index = int(row[1]) - 1
			end_index = int(row[2])
			length = int(row[3])
			tmp_s1 = out_l[:start_index]
			tmp_s2 = out_l[end_index:]
			if "Helix" == row[0]:
				tmp_new = "h" * length
			elif "Beta strand" == row[0]:
				tmp_new  = "b" * length
			else:
				tmp_new = "_" * length

			out_l = tmp_s1 + tmp_new + tmp_s2

		return out_l
					
	def get_secodary_struct_from_uniprot(self, p, p_seq):
		if p.find('|') != -1:
			p = p.split('|')[1]
		p = self.get_name_for_p_form_uniprot(p, p_seq)
		url = "http://www.uniprot.org/uniprot/"+p
		params = None
		
		data_num_tries_ls = self.__get_data_from_web__(url, params)
			
		data = data_num_tries_ls[0]
		num_of_tries = data_num_tries_ls[1]

		parser = myHTMLParser()
		parser.feed(data)
		if parser.secondary_structure:
			sec_struct_l = self.__convert_uniprot_format_to_l__(
				parser.secondary_structure, len(p_seq))
			return sec_struct_l, num_of_tries
		return None, num_of_tries
	

	#TODO the queue might not work as easily, also change seq to another name,
	# make the the p_to_seq_dict a Queue
	def download_p_sec_struct_from_uniprot(self, out_f_shared_obj):
		print "start downloading the secondary structure"
		try:
			#out_f = open(self.out_path, 'w')
			current = 0 
			out_f_shared_obj.set_total_num (len(self.proteins))
			#for p in self.proteins:
			while not self.p_to_seq_dict.empty():
				p = self.p_to_seq_dict.get()

				if len(p) != 1:
					raise Exception('Length of each dictionary in ' + 
								 'p_to_seq_dict has to be one.')

				for (k,v) in p.items():
					p_name = k
					p_seq = v
				params = None
				
				data_num_tries_ls = self.get_secodary_struct_from_uniprot(
										p_name, p_seq)
				# The website just gives us string of data not an html file
				# for humans
				sec_struct_l = data_num_tries_ls[0]
				if sec_struct_l == None:
					sec_struct_l = "NoteFound"
				num_of_tries = data_num_tries_ls[1]
				#########
				print out_f_shared_obj.write_to_file(p_name, sec_struct_l,
											  num_of_tries, True)
				#current = out_f_shared_obj.get_cur_p_num()
				#########
			#out_f_shared_obj.close_file()
		except IOError, e:
			print 'Some error while openning file happend', e


#End of class download_dict



class shared_type(object):
	def __init__(self,out_path,total=0):
		self.cur_p_num= 0
		self.__out_path = out_path
		self.out_f = None
		self.total = total

	def write_to_file(self, p, value_to_write, num_of_tries, 
				   return_state=False):
		try:
			if self.out_f == None:
				self.out_f = open(self.__out_path, 'w')
			self.cur_p_num = self.cur_p_num + 1
			self.out_f.write(p + '\t' + value_to_write +'\r\n')
		except IOError, e:
			print 'Some error while openning file happend', e

		if return_state:
			state = '_________________________________\r\n'
			if(num_of_tries == 1):
				state +=  (str(num_of_tries) + 
						 'st try success for protein: '+ p +'\n')
			else:
				state +=  ('>>>>>>>>>>>>>>>>>>>>' + str(num_of_tries) + 
						 ' TRY For protein: '+ p +'\n')

			percent = self.get_cur_p_num() * 100.0 / self.get_total_num()
			state +=  (value_to_write[1:30] + ' ' + 
					 str(self.get_cur_p_num()) + ' out of ' + 
					 str(self.get_total_num()) + '(' +  
					 str(round(percent,2)) +'%)' + '\r\n')
			state += '_________________________________\r\n'
			return state
		return 'You did not ask fo state'
			
	
	def close_file(self):
		if self.out_f != None:
			try:
				self.out_f.close()
			except IOError, e:
				print 'Some error while openning file happend', e
	
	def get_cur_p_num(self):
		return self.cur_p_num

	def set_total_num(self, total):
		self.total = total 

	def get_total_num(self):
		return self.total

class MyManager(managers.BaseManager):
	pass

MyManager.register('shared_file', shared_type)

if __name__ == "__main__":
	
	in_dict_path = os.path.join(
		os.path.abspath('../data'),'p_seq_dict_yeast_154828_formatted.txt')
	#in_dict_path = os.path.join(
		#os.path.abspath('../data'),'p_seq_dict_human_36134_formatted.txt')

	in_path = os.path.join(os.path.abspath('.'),'yeast_154828.txt')
	out_path = os.path.join(
		os.path.abspath('.'),'yeast_p_sec_struct_dict__formatted.txt')
	#out_path = os.path.join(
		#os.path.abspath('.'),'human_p_sec_struct_dict__formatted.txt')

	m_manager = MyManager()	
	m_manager.start()
	file_writer = m_manager.shared_file(out_path)

	get_dict = Download_dict(in_path,out_path, in_dict_path = in_dict_path)

	#get_dict.download_p_sec_struct_from_uniprot(file_writer)
		
	procs = []
	for i in range(16):
		p = Process(target=get_dict.download_p_sec_struct_from_uniprot,
				  args=(file_writer, ))
		p.daemon = True
		procs.append(p)
	
	for p in procs:
		p.start()
	for p in procs:
		p.join()
	is_alive = False	
	for p in procs:
		if p.is_alive():
			is_alive = True
			break
	if not is_alive:
		file_writer.close_file()

	"""
	in_path = os.path.join(os.path.abspath('.'),'yeast_154828.txt')
	out_path = os.path.join(
		os.path.abspath('.'),'p_seq_dict_yeast_154828_formatted.txt')

	m_manager = MyManager()	
	m_manager.start()
	file_writer = m_manager.shared_file(out_path)


	get_dict = Download_dict(in_path,out_path)
	get_dict.get_yeast_p_name()

	#procs = [Process(target=get_dict.download_yeast_p_seq_dict,
				   args=(file_writer, )) for i in range(18)]
	procs = []
	for i in range(24):
		p = Process(target=get_dict.download_yeast_p_seq_dict,
				  args=(file_writer, ))
		p.daemon = True
		procs.append(p)
	
	for p in procs:
		p.start()
	for p in procs:
		p.join()
	is_alive = False	
	for p in procs:
		if p.is_alive():
			is_alive = True
			break
	if not is_alive:
		file_writer.close_file()

	#A comment Test
	# humans website
	in_path = os.path.join(os.path.abspath('.'),'human_36134.txt')
	out_path = os.path.join(
		os.path.abspath('.'),'p_seq_dict_human_36134_formatted.txt')
	get_dict = Download_dict(in_path,out_path)
	get_dict.get_human_p_name()

	m_manager = MyManager()	
	m_manager.start()
	file_writer = m_manager.shared_file(out_path)

	get_dict = Download_dict(in_path,out_path)
	get_dict.get_yeast_p_name()
	procs = []
	for i in range(16):
		p = Process(target=get_dict.download_human_p_seq_dict,
				  args=(file_writer, ))
		p.daemon = True
		procs.append(p)
	
	for p in procs:
		p.start()
	for p in procs:
		p.join()
	is_alive = False	
	for p in procs:
		if p.is_alive():
			is_alive = True
			break
	if not is_alive:
		file_writer.close_file()
	
	# humans website
	in_path = os.path.join(os.path.abspath('.'),'human_27307.txt')
	out_path = os.path.join(
		os.path.abspath('.'),'p_seq_dict_human_27307_formatted.txt')
	get_dict = Download_dict(in_path,out_path)
	get_dict.get_human_p_name()

	m_manager = MyManager()	
	m_manager.start()
	file_writer = m_manager.shared_file(out_path)

	get_dict = Download_dict(in_path,out_path)
	get_dict.get_yeast_p_name()
	procs = []
	for i in range(43):
		p = Process(target=get_dict.download_human_p_seq_dict,
				  args=(file_writer, ))
		p.daemon = True
		procs.append(p)
	
	for p in procs:
		p.start()
	for p in procs:
		p.join()
	is_alive = False	
	for p in procs:
		if p.is_alive():
			is_alive = True
			break
	if not is_alive:
		file_writer.close_file()
	"""
