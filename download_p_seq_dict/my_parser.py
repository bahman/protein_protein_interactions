from HTMLParser import HTMLParser

class myHTMLParser(HTMLParser):

	def __init__(self):
		#This is how to call old super since they don't extend object class
		HTMLParser.__init__(self)
		self.__is_pre__ = False
		self.__is_search_table__ = False
		self.__is_td__ = False
		self.__is_tr__ = False
		self.__is_secondary_tbody__ = False
		#For the table inside the secondary structure detail table to be ignored
		self.__is_inner_table__ = False
		self.sequence  = None
		self.search_results = []
		self.secondary_structure = []
		self.list_of_td = []



	##########################################################
	#Function which handles start tags
	##########################################################
	def handle_starttag(self, tag , attrs):
		if(tag == 'pre'):
			self.__is_pre__ = True

		if(tag == 'table'):
			for attr in attrs:

				#Search of sequence
				if attr[0] == 'id' and attr[1] == 'results':
					self.__is_search_table__ = True

			#Secondary structure checks if it reaches the inner table
			if self.__is_secondary_tbody__:
				self.__is_inner_table__ = True

		if(tag == 'tbody'):
			for attr in attrs:
				if attr[0] == 'id' and attr[1] == 'secondary-structure-details':
					self.__is_secondary_tbody__ = True
			#print "Encountered the beginning of a %s tag" % tag

		#Search of sequence
		if(self.__is_search_table__ and tag == 'td'):
			self.__is_td__ = True

		#Search of sequence
		if(self.__is_search_table__ and tag == 'tr'):
			self.__is_tr__ = True
			self.list_of_td = []

		#Secondary structure
		if(self.__is_secondary_tbody__ and tag == 'td' and not self.__is_inner_table__):
			self.__is_td__ = True

		#Secondary structure
		if(self.__is_secondary_tbody__ and tag == 'tr' and not self.__is_inner_table__):
			self.__is_tr__ = True
			self.list_of_td = []


	##########################################################
	#Function which handles end tags
	##########################################################
	def handle_endtag(self, tag):

		if(tag == 'pre'):
			self.__is_pre__ = False

		if(tag == 'table'):
			self.__is_search_table__ = False
			if self.__is_inner_table__:
				self.__is_inner_table__ = False

		if(tag == 'tbody'):
			self.__is_secondary_tbody__ = False

		#Search of sequence
		if(self.__is_search_table__ and tag == 'td'):
			self.__is_td__ = False

		#Search of sequence
		if(self.__is_search_table__ and tag == 'tr'):
			self.search_results.append(self.list_of_td)
			self.__is_tr__ = False

		#Secondary structure
		if(self.__is_secondary_tbody__ and tag == 'td'):
			self.__is_td__ = False

		#Secondary structure not that the self.__is_inner_table__ is used to prevent duplicate added enteris
		if(self.__is_secondary_tbody__ and tag == 'tr' and not self.__is_inner_table__):
			self.secondary_structure.append(self.list_of_td)
			self.__is_tr___ = False



	##########################################################
	#Function which handles data
	##########################################################
	def handle_data(self,data):
		if self.__is_pre__:
			self.sequence = data

		#Search of sequence
		if self.__is_td__:
			self.list_of_td.append(data)


parser = myHTMLParser()
