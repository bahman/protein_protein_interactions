import os
import sys
import re

from p_code_input_output.c_data_set import SequenceManipulation

class secStructManipulation(SequenceManipulation):
	
	def __init__(self, **kwargs):
		
		super(secStructManipulation, self).__init__(**kwargs)
		# Secondary structure dictionary for conjoint triad
		self.sec_con_tri = {}
		self._reset_sec_con_tri__()
		self.psec_dict = {}

		self.sec_sec_i_list = []
		self.ssi_for_sec_list  = [] #For protein pairs WITH Sec struct

		self.sec_sec_i_p = kwargs.get('sec_sec_i_p', None)
		self.ssi_for_sec_p = kwargs.get('ssi_for_sec_p', None) 

	def _reset_sec_con_tri__(self):
		"""  resets all the values of the dictionary of the frequencies to
			zero
			
		""" 
		for i in range(1,8):
			for j in range(1,8):
				for v in range(1,8):
					k = str(i) + str(j) + str(v)
					self.sec_con_tri[k] = {'h':0 , 'b':0 , '_':0} 

	def r_psec_dict_from_f(self, sep='\t', **kwargs):
		"""  Fills the p_to_sec_struct_dict with proteins and their secondary
			structure

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			p_sec_p -- the path from which the dictionray needs to be read 
					 from

		"""
		p_sec_p = kwargs.get('p_sec_p', self.p_sec_p)
		if p_sec_p is None:
			err_mes = "The path to p_sec is wrong.[r_psec_dict_from_f]\n"
			sys.stderr.write (err_mes)
			return 0

		try:
			print 'Creating p_sec dictionary'
			p_sec_f = open(p_sec_p, 'r')
			for line in p_sec_f:
				b_line = line.split(sep)
				p = b_line[0]
				sec_struct = b_line[1]
				self.psec_dict[p] = sec_struct
			print 'Dictionary of p_sec is made'
			p_sec_f.close()
		except IOError,e:
			print 'cannot open the file',e
	
	#TODO Make it shorter, it is very long, but it basically does the same
	# thing as f_ssi_list but addes another thing for sec struct
	def f_sec_sec_i_list(self, **kwargs):
		"""  Fills the sec_sec_i_list dictionary and if sec_sec_i_p is 
			given, it will write the dictionary to that path. Fills 
			ssi_list and ssi_for_sec_list.

			Arguments:
			sec_sec_i_p -- The path to write the sec_sec_i to
			ssi_for_sec_p -- The path for ssi of the corresonding (or 
						  existing) secondary structure pairs

			Note: The ppi_list should be filled previously

		"""
		if self.ppi_list == []:
			err_mes = "The ppi list need to be filled.[f_sec_sec_i_list]\n"
			sys.stderr.write (err_mes)
			return 0
		sec_sec_i_p = kwargs.get('sec_sec_i_p', self.sec_sec_i_p)
		ssi_for_sec_p = kwargs.get('ssi_for_sec_p', self.ssi_for_sec_p)
		sec_sec_i_f = None
		ssi_for_sec_f = None
		try:
			if sec_sec_i_p is not None:
				sec_sec_i_f =  open(sec_sec_i_p, 'w')
			if ssi_for_sec_p is not None:
				ssi_for_sec_f = open(ssi_for_sec_p, 'w')	
			p = re.compile('[\r\n, \n]')
			#This dict is used for checking what ppis have the same sequence
			tmp_sec_sec_i_dict = {}
			tmp_ssi_dict = {}
			for cnt,ppi in enumerate(self.ppi_list):
				for (k1,k2),v in ppi.items():
					if  (k1 not in self.psec_dict or 
						k2 not in self.psec_dict):
						continue
					sec1 = p.sub ('', self.psec_dict[k1])
					sec2 = p.sub ('', self.psec_dict[k2])
					s1 = p.sub ('', self.ps_dict[k1])
					s2 = p.sub ('', self.ps_dict[k2])
					val = v
				err_mes = ''
				if sec1 == 'NotFound' or sec2 == 'NotFound':
					err_mes = (str(cnt) + ' ' + k1 + ' ' + k2 + ' ' + 
							"sec struct for 1 not found\n")
				elif s1 == 'NotFound' or s2 == 'NotFound':
					err_mes = (str(cnt) + ' ' + k1 + ' ' + k2 + ' ' + 
							"sequnce for one of them was not found\n")
				if err_mes is not '':
					sys.stderr.write (err_mes)
					continue

				val = p.sub ('', val)
				# TODO Fix the ones that are duplicates
				# Please note that the 30 sequences are not retrived 
				# properly from the database and they are printed to to err
				if (sec1, sec2) in tmp_sec_sec_i_dict:
					err_mes = (str(cnt) + ' ' + k1 + ' ' + k2 + ' ' + 
							"SAME SECSTRUCT\n")
				elif (s1, s2) in tmp_ssi_dict:
					err_mes += (str(cnt) + ' ' + k1 + ' ' + k2 + ' ' + 
							"SAME SEQUENCE\n")
				else:
					if sec_sec_i_f is not None:
						ssi_l = sec1 + ' ' +  sec2 + ' ' + val + '\r\n'
						sec_sec_i_f.write(ssi_l)
					if ssi_for_sec_f is not None:
						ssi_l = s1 + ' ' +  s2 + ' ' + val + '\r\n'
						ssi_for_sec_f.write(ssi_l)
					
					self.sec_sec_i_list.append( {(sec1, sec2) : val} )
					self.ssi_for_sec_list.append( {(s1, s2) : val} )
					tmp_sec_sec_i_dict [sec1, sec2] = val
					tmp_ssi_dict [s1, s2] = val

				sys.stderr.write (err_mes)
				
			if sec_sec_i_f is not None:
				sec_sec_i_f.close()
			if ssi_for_sec_f is not None:
				ssi_for_sec_f.close()
			del tmp_sec_sec_i_dict
			del tmp_ssi_dict
			# NOTE THIS LINE MAKES cci AND ffi LIST BE OF THE SIZE OF SEC
			# STRUCT OR ssi_FOR_SEC
			self.ssi_list = self.ssi_for_sec_list
		except IOError,e:
			print 'cannot open the file',e

	def _sec_find_freq(self, c_text, sec_text=None):
		"""  Returns an array of conjoint_triad which basically shows the 
			number of frequencies """

		matches = re.finditer(r'(?=([1 | 2 | 3 | 4 | 5 | 6]{3}))',c_text)
		#results = [match.group(1)  for match in matches]
		if sec_text is not None:
			for match in matches:
				c = match.group(1)
				corres_sec = sec_text[match.start(1)+1]
				self.sec_con_tri [c] [corres_sec] +=  1

		matches = re.finditer(r'(?=([1 | 2 | 3 | 4 | 5 | 6]{3}))',c_text)
		results = [match.group(1)  for match in matches]
		self._reset_con_tri()
		for freq in results:
			freq = str(freq)
			self.con_tri[freq] = self.con_tri[freq] + 1
		return self.con_tri, self.sec_con_tri

	def _sort_con_tri(self,con_tri):
		keys = con_tri.keys()
		keys.sort()
		return map(con_tri.get, keys)

	def _sort_sec_con_tri(self,sec_tri):
		keys = sec_tri.keys()
		keys.sort()
		return map(sec_tri.get, keys)

	def sub_c_with_f(self, sep=' ', **kwargs):
		"""  Substitutes amino classes with frequencies and in the sorted 
			form from 111 to 777

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			sec_sec_i_p -- is the path to read sec-sec-i from
			cci_p -- is the path to amino acids class file to read 
			ffi_p -- is the path to write the output to	
			call_parent -- is boolean which uses the parent method

		"""
		call_parent = kwargs.get('call_parent', False)
		if call_parent:
			r = super(secStructManipulation, self).sub_c_with_f(sep
													  , **kwargs)
			return r
		sec_sec_i_p = kwargs.get('sec_sec_i_p', self.sec_sec_i_p)
		cci_p = kwargs.get('cci_p', self.cci_p)
		ffi_p = kwargs.get('ffi_p', self.ffi_p)
		sec_sec_i_f = None
		cci_f = None
		ffi_f = None


		try:
			err_mes = ""

			if self.cci_list != []:
				cci_s = self.cci_list	
			else:
				if cci_p is None:
					err_mes = "The path to cci is wrong.[sub_c_with_f]\n"
				else:
					cci_f = open(cci_p, 'r')
					cci_s = cci_f

			if self.sec_sec_i_list != []:
				sec_sec_i_s = self.sec_sec_i_list
			else:
				if sec_sec_i_p is None:	
					err_mes = ("The path to sec_sec_i is wrong." + 
							 "[sub_c_with_f]\n")
				else:
					sec_sec_i_s = []
					sec_sec_i_f= open(sec_sec_i_p, 'r')
					for line in sec_sec_i_f:
						sec_sec_i_s.append(line)

			if err_mes is not "":
				sys.stderr.write (err_mes)
				return 0

			if ffi_p is not None:
				ffi_f = open(ffi_p,'w')	

			print 'Start substituding classes with frequencies'

			for line_i, cci in enumerate(cci_s):
				if type(cci) == dict:
					for (c1, c2), val in cci.items():
						cci = c1 + ' ' + c2 + ' ' + val
				if type(sec_sec_i_s[line_i]) == dict:
					for (sec1, sec2), val in sec_sec_i_s[line_i].items():
						sec_sec_i = sec1 + ' ' + sec2 + ' ' + val
				else:
					sec_sec_i = sec_sec_i_s[line_i]

				b_line = re.split(sep, cci)
				b_sec_line = re.split(sep, sec_sec_i)

				self._reset_con_tri()
				self._reset_sec_con_tri__()

				f_1, sec_1 = self._sec_find_freq(b_line[0], b_sec_line[0])
				sort_f_1 = self._sort_con_tri(f_1)	
				sort_sec_1 = self._sort_sec_con_tri(sec_1)
				tmp_sec_1 = ''
				for sec_freq in sort_sec_1:
					for sec, f in sec_freq.items():
						tmp_sec_1 += str(f) + ' '
				sort_sec_1 = tmp_sec_1[:-1]

				f_2, sec_2 = self._sec_find_freq(b_line[1], b_sec_line[1])
				sort_f_2 = self._sort_con_tri(f_2)	
				sort_sec_2 = self._sort_sec_con_tri(sec_2)
				tmp_sec_2 = ''
				for sec_freq in sort_sec_2:
					for sec, f in sec_freq.items():
						tmp_sec_2 += str(f) + ' '
				sort_sec_2 = tmp_sec_2[:-1]

				# Adding the results of interacting or non-interacting to 
				# the last element 
				sort_f_1 = map(lambda a: str(a), sort_f_1)
				sort_f_2 = map(lambda a: str(a), sort_f_2)
				sort_f_1 = ' '.join(sort_f_1)
				sort_f_2 = ' '.join(sort_f_2)
				if ffi_f is not None:
					ffi_f.write(sort_f_1+ ' ' + sort_sec_1  + ' ' + 
							  sort_f_2 + ' ' + sort_sec_2 + ' ' +
							  b_line[2])

				self.ffi_list.append( {(sort_f_1, sort_sec_1, sort_f_2, 
								    sort_sec_2): b_line[2]} )
			print 'Finished substituding classes with frequencies'
			if ffi_f is not None:
				ffi_f.close()
			if cci_f is not None:
				cci_f.close()
		except IOError,e:
			print 'cannot open the file',e
		return 1

	def normalize(self, sep= ' ', **kwargs):
		""" This function normalizes the output of frequencies so they would be 
		    between 0 nand 1

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			ffi_p -- is the path frequencies to read
			nni_p -- the out path to normalized frequencies
			call_parent -- is boolean which uses the parent method

		"""
		call_parent = kwargs.get('call_parent', False)
		if call_parent:
			r = super(secStructManipulation, self).normalize(sep, **kwargs)
			return r
		ffi_p = kwargs.get('ffi_p', self.ffi_p)
		nni_p = kwargs.get('nni_p', self.nni_p)
		ffi_f = None
		nni_f = None
		try:
			if nni_p is not None:
				nni_f = open(nni_p, 'w')

			if self.ffi_list != []:
				ffi_s = self.ffi_list
			else:
				if ffi_p is None:
					err_mes = "The path to ffi is wrong.[normalize]\n"
					sys.stderr.write (err_mes)
					return 0
				ffi_f = open(ffi_p, 'r')
				ffi_s = ffi_f

			print '\nstart normalization'
			for ffi in ffi_s:
				if type(ffi) == dict:
					for (f1, sec1, f2, sec2), val in ffi.items():
						ffi = (f1 + ' ' + sec1 + ' ' + f2 + ' ' + sec2 + ' ' +
								val)

				ffi = ffi.split(' ')
				for i, x in enumerate(ffi):
					if x is '':
						print i, x

				ffi = map(lambda x: float(x), ffi)
				max_f = max(ffi)
				min_f = min(ffi)

				norm_ffi = map(lambda x: (x - min_f) / (max_f), ffi[:-1])

				norm_ffi.append(ffi[-1])
				str_norm_ffi = map(lambda x : str(x), norm_ffi)
				n_len = (len(norm_ffi) - 1) / 4
				n1 = str_norm_ffi[:n_len]	
				n_sec1 = str_norm_ffi[n_len:2*n_len]	
				n2 = str_norm_ffi[2*n_len:3*n_len]	
				n_sec2 = str_norm_ffi[n_len:4*n_len]	
				val = str_norm_ffi[-1]
				n1 = ' '.join(n1)
				n_sec1 = ' '.join(n_sec1)
				n2 = ' '.join(n2)
				n_sec2 = ' '.join(n_sec2)
				self.nni_list.append({(n1, n_sec1, n2, n_sec2) : val})
				if nni_f is not None:
					nni_f.write(' '.join(str_norm_ffi) + '\n')

			print 'finished normalization\n'
			if nni_f is not None:
				nni_f.close()

		except IOError,e:
			sys.stderr('cannot open the file' + str(e) + '\n')
		return 1


if __name__ == '__main__':
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				'yeast_p_sec_struct_dict__formatted.txt'
			)
	ppi_p =  os.path.join(os.path.abspath('.'), 'yeastP1.txt')
	ppi_p =  os.path.join(os.path.abspath('.'), 'tmp_set.txt')
	name = ('yeast','P1')
	# ppi_p =  os.path.join(os.path.abspath('.'), 'yeastN_random1.txt')
	# name = ('yeast','rN1')
	ssi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_ssi.txt'
			)
	cci_p =   os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_cci.txt'
			)
	ffi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_ffi.txt'
			)
	nni_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_nni.txt'
			)
	libsvm_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_libsvm_iff.txt'
			)

	sec_sec_i_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_sec_sec_i.txt'
			)
	ssi_for_sec_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_ssi_for_sec.txt'
			)

	ffi_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_ffi.txt'
			)
	nni_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_nni.txt'
			)
	libsvm_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_libsvm_iff.txt'
			)
	seqManip = secStructManipulation(
				p_seq_p=p_seq_p,
				p_sec_p=p_sec_p,
				ppi_p=ppi_p,
				ssi_p=ssi_p, # Fills writes ssi list to the path
				sec_sec_i_p=sec_sec_i_p,
				ssi_for_sec_p=ssi_for_sec_p,
				cci_p=cci_p,
				ffi_p=ffi_p,
				nni_p=nni_p,
				libsvm_p = libsvm_p,
			)
	seqManip.r_ps_dict_from_f()
	seqManip.r_psec_dict_from_f()
	seqManip.r_ppi_list_from_f();
	seqManip.f_ssi_list();
	#Since it is called before sub of cci the new matrix would be for sec
	seqManip.f_sec_sec_i_list()
	seqManip.f_cci_sub_s_with_c()
	#Note the following is done only so that the same data is not appended to
	#the ffi list at the end of the function (for future use)
	# For 686 (parent) feature
	seqManip.sub_c_with_f(call_parent=True, ffi_p = ffi_parent_p)
	seqManip.normalize(call_parent=True, nni_p = nni_parent_p)
	seqManip.libsvm_format(nni_p = nni_parent_p, libsvm_p=libsvm_parent_p)
	# For SEC struct
	seqManip.ffi_list = []
	seqManip.sub_c_with_f()
	seqManip.normalize()
	seqManip.libsvm_format()
	print len(seqManip.ps_dict)
	print len(seqManip.psec_dict)
	print len(seqManip.ppi_list)
	print len(seqManip.ssi_list)
	print len(seqManip.cci_list)
	print len(seqManip.ffi_list)
