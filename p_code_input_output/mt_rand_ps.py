from p_code_input_output.data_set import count_lines

ssh_workers = (
			['zombie'] * 5 +
			['lich'] * 5 +
			['shark'] * 5 +
			['skeleton'] * 5 +
			['ghoul'] * 5 +
			['whale'] * 5 +
			['porpoise'] * 5 +
			['seal'] * 5 +
			['banshee'] * 1 +
			['bass'] * 5 +
			['butterfish'] * 5 +
			['bream'] * 5 +
			['chard'] * 5 +
			['clam'] * 5 +
			['conch'] * 5 +
			['walrus'] * 5 +
			['manatee'] * 5 +
			['mummy'] * 5 +
			['wraith'] * 5 +
			['kelp'] * 5 +
			['octopus'] * 5 +
			['eel'] * 5 +
			['pumpkin'] * 5 +
			['potato'] * 5 +
			['cockle'] * 5 +
			['seacucumber'] * 5 +
			['seapineapple'] * 5 +
			['crab'] * 5 +
			['crayfish'] * 5 +
			['cuttlefish'] * 5 +
			['blowfish'] * 5 +
			['bluefish'] * 5 +
			['celery'] * 5 +
			['pignut'] * 5 +
			['parsnip'] * 5 +
			['swede'] * 5 +
			['sweetpepper'] * 5 +
			['sprout'] * 5 +
			['spinach'] * 5 +
			['brill'] * 5 
			)
nr_local_worker = 5


def get_num_to_rm(res_p):
	num_p_l = []
	acc_l = []
	all_d = {}

	try:
		res_f = open(res_p, 'r')
		for l in res_f:
			l_arr = l.split()
			num_p_l.append(l_arr[-1])
			if l_arr[-1] not in all_d:
				all_d[l_arr[-1]] = l_arr[-2]
			else:
				raise KeyError('num_p already exist' + str(l_arr[-1]))
	except IOError:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
									  sys.exc_info()[2])
	num_p_l.sort()
	acc_l = map(all_d.get, num_p_l)
	return num_p_l, acc_l


class WorkerStopToken:  # used to notify the worker to stop
		pass


class DatasetWorker(Thread):
	def __init__(self,name,job_queue,result_queue):
		Thread.__init__(self)
		self.name = name
		self.job_queue = job_queue
		self.result_queue = result_queue
	def run(self):
		while True:
			num_rm = self.job_queue.get()
			if num_rm is WorkerStopToken:
				###print 'closing ' + self.name
				self.job_queue.put(num_rm)
				# print('worker {0} stop.'.format(self.name))
				break
			try:
				print self.name + ' Running ' + num_rm
				success = self.run_one(num_rm)
				if success is None: raise RuntimeError("Was not successful")
			except:
				# we failed, let others do that and we just quit
				traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
				self.job_queue.put(num_rm)
				print('worker {0} quit.'.format(self.name))
				break
			else:
				self.result_queue.put((self.name, num_rm[-1]))


class SSHDatasetWorker(DatasetWorker):
	def __init__(self,name,job_queue,result_queue,host, ppi_p):
		DatasetWorker.__init__(self,name,job_queue,result_queue)
		self.host = host
		self.cwd = os.getcwd()
		self.ppi_p = ppi_p
		self.py_exe = 'python'
		self.py_code = 'rm_num_p_from_file.py'
	def run_one(self, num_rm):
		py_path = "export PYTHONPATH=/home/ba2g09/workspace/summer_internship;"
		cmd_str = 'ssh -x {0} "' + py_path + 'cd {1}; {2} {3} -i {4} -n {5}"'
		cmdline = cmd_str.format(self.host, self.cwd, self.py_exe, self.py_code,
						self.ppi_p , num_rm)
		print cmdline
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		for line in result.readlines():
			if str(line).find("Finish changing the format to libsvm") != -1:
				return True


class LocalDatasetWorker(DatasetWorker):
	def __init__(self,name,job_queue,result_queue, ppi_p):
		DatasetWorker.__init__(self,name,job_queue,result_queue)
		self.ppi_p = ppi_p
		self.py_exe = 'python'
		self.py_code = 'rm_num_p_from_file.py'
	def run_one(self, num_rm):
		cmd_str = '{0} {1} -i {2} -n {3}'
		cmdline = cmd_str.format(self.py_exe, self.py_code, self.ppi_p, 
								num_rm)
		print cmdline
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		for line in result.readlines():
			if str(line).find("Finish changing the format to libsvm") != -1:
				return True


def mt_rm_large_ps(ppi_p, num_ps_l):

	global ssh_workers, nr_local_worker
	ret_ppi_lists_d = {}
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				'yeast_p_sec_struct_dict__formatted.txt'
			)
	seqManip = SequenceManipulation(
					p_seq_p=p_seq_p,
					p_sec_p=p_sec_p,
					ppi_p=ppi_p,
			   )
	seqManip.r_ps_dict_from_f()
	seqManip.r_ppi_list_from_f();
	ppi_l = seqManip.ppi_list

	# put jobs in queue
	total_jobs = 0
	job_queue = Queue.Queue(0)
	result_queue = Queue.Queue(0)
	jobs = []

	p_removed = []
	for tmp_p_freq in top_p_freq:
		for p, freq in tmp_p_freq.items():
			pass
		total_jobs += 1
		p_removed.append(p)
		#jobs.append(p)
		jobs.append(list(p_removed))
		#job_queue.put(p)
		job_queue.put(list(p_removed))
		print p

	#Connect to hosts first
	hostnames = set(ssh_workers) #removing the extras
	hostnames = list(hostnames)

	password = getpass.getpass('Password:')
	if password != '':
		ssh_connector = ssh_connection.SSHConnection(hostnames_l=hostnames,
						password=password)
		# make connection to ssh workers first
		ssh_connector.conn_to_clients_wait()
		ssh_workers = ssh_connector.conn_l
		random.shuffle(ssh_workers)
		# fire ssh workers
		if ssh_workers:
			for host in ssh_workers:
				SSHDatasetWorker(host,job_queue,result_queue,host,ppi_p).start()

	for num in range(nr_local_worker):
		LocalDatasetWorker(str(num), job_queue, result_queue, ppi_p).start()

	done_jobs = {}
	#result_f = open('_'.join(p_removed)+'.txt', 'w')
	result_p = str(len(p_removed)) + '_results_cur.out' 
	result_f = open(result_p, 'w')

	for p  in jobs:
		#while p not in done_jobs:
		print '^'*50
		print 'Came for', p
		#NOTE that get waites untile the queue has an item in it
		(worker, p) = result_queue.get()
		print 'Got', p
		print '_'*50
		done_jobs[p] = 1
		#cnt_p = count_lines(p+'.txt') #Num of proteins
		cnt_p = count_lines(p+'.txt.tar.gz') #Num of proteins
		result_f.write('{0} {1} {2} \n'.format(worker, p, cnt_p))
		result_f.flush()

		nr_done_jobs = len(done_jobs)
		perc = nr_done_jobs * 100.0 / total_jobs
		perc = round(perc, 3)
		state = ("["+ str(nr_done_jobs) + " out of " + str(total_jobs) +
					" " + str(perc)+"%]")
		result = '[{0}] {1} {2}'.format(worker, p, cnt_p)
		print(result + "|" + state)

	job_queue.put(WorkerStopToken)
	if password != '':
			ssh_connector.close_clients()

	return ret_ppi_lists_d
