import os
import sys
import re
import traceback

from p_code_input_output.c_secondary_data_set import secStructManipulation

class secStructOnlyManipulation(secStructManipulation):
	
	def sub_c_with_f(self, sep=' ', **kwargs):
		"""  Substitutes amino classes with frequencies and in the sorted 
			form from 111 to 777

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			sec_sec_i_p -- is the path to read sec-sec-i from
			cci_p -- is the path to amino acids class file to read 
			ffi_p -- is the path to write the output to	
			call_parent -- is boolean which uses the parent method

		"""
		call_parent = kwargs.get('call_parent', False)
		if call_parent:
			r = super(secStructOnlyManipulation, self).sub_c_with_f(sep
													  , **kwargs)
			return r
		sec_sec_i_p = kwargs.get('sec_sec_i_p', self.sec_sec_i_p)
		cci_p = kwargs.get('cci_p', self.cci_p)
		ffi_p = kwargs.get('ffi_p', self.ffi_p)
		sec_sec_i_f = None
		cci_f = None
		ffi_f = None


		try:
			err_mes = ""

			if self.cci_list != []:
				cci_s = self.cci_list	
			else:
				if cci_p is None:
					err_mes = "The path to cci is wrong.[sub_c_with_f]\n"
				else:
					cci_f = open(cci_p, 'r')
					cci_s = cci_f

			if self.sec_sec_i_list != []:
				sec_sec_i_s = self.sec_sec_i_list
			else:
				if sec_sec_i_p is None:	
					err_mes = ("The path to sec_sec_i is wrong." + 
							 "[sub_c_with_f]\n")
				else:
					sec_sec_i_s = []
					sec_sec_i_f= open(sec_sec_i_p, 'r')
					for line in sec_sec_i_f:
						sec_sec_i_s.append(line)

			if err_mes is not "":
				sys.stderr.write (err_mes)
				return 0

			if ffi_p is not None:
				ffi_f = open(ffi_p,'w')	

			print 'Start substituding classes with frequencies'

			for line_i, cci in enumerate(cci_s):
				if type(cci) == dict:
					for (c1, c2), val in cci.items():
						cci = c1 + ' ' + c2 + ' ' + val
				if type(sec_sec_i_s[line_i]) == dict:
					for (sec1, sec2), val in sec_sec_i_s[line_i].items():
						sec_sec_i = sec1 + ' ' + sec2 + ' ' + val
				else:
					sec_sec_i = sec_sec_i_s[line_i]

				b_line = re.split(sep, cci)
				b_sec_line = re.split(sep, sec_sec_i)

				self._reset_con_tri()
				self._reset_sec_con_tri__()

				f_1, sec_1 = self._sec_find_freq(b_line[0], b_sec_line[0])
				sort_f_1 = self._sort_con_tri(f_1)	
				sort_sec_1 = self._sort_sec_con_tri(sec_1)
				tmp_sec_1 = ''
				for sec_freq in sort_sec_1:
					for sec, f in sec_freq.items():
						tmp_sec_1 += str(f) + ' '
				sort_sec_1 = tmp_sec_1[:-1]

				f_2, sec_2 = self._sec_find_freq(b_line[1], b_sec_line[1])
				sort_f_2 = self._sort_con_tri(f_2)	
				sort_sec_2 = self._sort_sec_con_tri(sec_2)
				tmp_sec_2 = ''
				for sec_freq in sort_sec_2:
					for sec, f in sec_freq.items():
						tmp_sec_2 += str(f) + ' '
				sort_sec_2 = tmp_sec_2[:-1]

				# Adding the results of interacting or non-interacting to 
				# the last element 
				sort_f_1 = map(lambda a: str(a), sort_f_1)
				sort_f_2 = map(lambda a: str(a), sort_f_2)
				sort_f_1 = ' '.join(sort_f_1)
				sort_f_2 = ' '.join(sort_f_2)
				if ffi_f is not None:
					ffi_f.write(sort_sec_1  + ' ' + sort_sec_2 + ' ' +
							  b_line[2])

				self.ffi_list.append( {(sort_sec_1, sort_sec_2): b_line[2]} )
			print 'Finished substituding classes with frequencies'
			if ffi_f is not None:
				ffi_f.close()
			if cci_f is not None:
				cci_f.close()
		except IOError,e:
			print 'cannot open the file',e
		return 1

	def normalize(self, sep= ' ', **kwargs):
		""" This function normalizes the output of frequencies so they would be 
		    between 0 nand 1

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			ffi_p -- is the path frequencies to read
			nni_p -- the out path to normalized frequencies
			call_parent -- is boolean which uses the parent method

		"""
		call_parent = kwargs.get('call_parent', False)
		if call_parent:
			r = super(secStructManipulation, self).normalize(sep, **kwargs)
			return r
		ffi_p = kwargs.get('ffi_p', self.ffi_p)
		nni_p = kwargs.get('nni_p', self.nni_p)
		ffi_f = None
		nni_f = None
		try:
			if nni_p is not None:
				nni_f = open(nni_p, 'w')

			if self.ffi_list != []:
				ffi_s = self.ffi_list
			else:
				if ffi_p is None:
					err_mes = "The path to ffi is wrong.[normalize]\n"
					sys.stderr.write (err_mes)
					return 0
				ffi_f = open(ffi_p, 'r')
				ffi_s = ffi_f

			print '\nstart normalization'
			for ffi in ffi_s:
				if type(ffi) == dict:
					for (sec1, sec2), val in ffi.items():
						ffi = (sec1 + ' ' + sec2 + ' ' + val)

				ffi = ffi.split(' ')
				for i, x in enumerate(ffi):
					if x is '':
						print i, x

				ffi = map(lambda x: float(x), ffi)
				max_f = max(ffi)
				min_f = min(ffi)

				norm_ffi = map(lambda x: (x - min_f) / (max_f), ffi[:-1])

				norm_ffi.append(ffi[-1])
				str_norm_ffi = map(lambda x : str(x), norm_ffi)
				n_len = (len(norm_ffi) - 1) / 2
				n_sec1 = str_norm_ffi[:n_len]	
				n_sec2 = str_norm_ffi[n_len:-1]	
				val = str_norm_ffi[-1]
				n_sec1 = ' '.join(n_sec1)
				n_sec2 = ' '.join(n_sec2)
				self.nni_list.append({(n_sec1, n_sec2) : val})
				if nni_f is not None:
					nni_f.write(' '.join(str_norm_ffi) + '\n')

			print 'finished normalization\n'
			if nni_f is not None:
				nni_f.close()

		except IOError,e:
			#sys.stderr('cannot open the file' + str(e) + '\n')
			traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
		return 1


if __name__ == '__main__':
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				'yeast_p_sec_struct_dict__formatted.txt'
			)
	ppi_p =  os.path.join(os.path.abspath('.'), 'p2_set.txt')
	name = ('yeast','P2')
	# ppi_p =  os.path.join(os.path.abspath('.'), 'r1_set.txt')
	# name = ('yeast','r1')
	ssi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_ssi.txt'
			)
	cci_p =   os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_cci.txt'
			)
	ffi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_ffi.txt'
			)
	nni_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_nni.txt'
			)
	libsvm_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_libsvm_iff.txt'
			)
	arff_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_arff_iff.txt'
			)

	sec_sec_i_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_sec_sec_i.txt'
			)
	ssi_for_sec_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_ssi_for_sec.txt'
			)

	ffi_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_ffi.txt'
			)
	nni_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_nni.txt'
			)
	libsvm_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_libsvm_iff.txt'
			)
	arff_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_arff_iff.txt'
			)
	seqManip = secStructOnlyManipulation(
				p_seq_p=p_seq_p,
				p_sec_p=p_sec_p,
				ppi_p=ppi_p,
				ssi_p=ssi_p, # Fills writes ssi list to the path
				sec_sec_i_p=sec_sec_i_p,
				ssi_for_sec_p=ssi_for_sec_p,
				cci_p=cci_p,
				ffi_p=ffi_p,
				nni_p=nni_p,
				libsvm_p = libsvm_p,
				arff_p = arff_p,
			)
	seqManip.r_ps_dict_from_f()
	seqManip.r_psec_dict_from_f()
	seqManip.r_ppi_list_from_f();
	seqManip.f_ssi_list();
	#Since it is called before sub of cci the new matrix would be for sec
	seqManip.f_sec_sec_i_list()
	seqManip.f_cci_sub_s_with_c()
	#Note the following is done only so that the same data is not appended to
	#the ffi list at the end of the function (for future use)
	# For 686 (parent) feature
	seqManip.sub_c_with_f(call_parent=True, ffi_p = ffi_parent_p)
	seqManip.normalize(call_parent=True, nni_p = nni_parent_p)
	seqManip.libsvm_format(nni_p = nni_parent_p, libsvm_p=libsvm_parent_p)
	seqManip.arff_format(nni_p = nni_parent_p, arff_p=arff_parent_p)
	# For SEC struct
	seqManip.ffi_list = []
	seqManip.sub_c_with_f()
	seqManip.normalize()
	seqManip.libsvm_format()
	seqManip.arff_format()
	print len(seqManip.ps_dict)
	print len(seqManip.psec_dict)
	print len(seqManip.ppi_list)
	print len(seqManip.ssi_list)
	print len(seqManip.cci_list)
	print len(seqManip.ffi_list)
