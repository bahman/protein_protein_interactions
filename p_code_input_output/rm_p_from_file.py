import getopt
import sys
import data_set

global input_f, p, verbose

def usage():
	print  """ To be done """

def process_options():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hvi:p:", ["help", "protein="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)
	global input_f, p, verbose
	input_f = None
	p = None
	verbose = False
	for o, a in opts:
		if o == "-v":
			verbose = True
		elif o in ("-h", "--help"):
			usage()
			sys.exit()
		#elif o in ("-p", "--p"):
			#p = a
		elif o in ("-i", "--input"):
			input_f = a
		else:
			assert False, "unhandled option"
	p = args


if __name__ == '__main__':
	process_options()
	print input_f, p, verbose
	data_set.rm_p_from_f_write(input_f, p)
