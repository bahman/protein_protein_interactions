import re
import os
from time import time

import numpy

from p_code_input_output.c_data_set import SequenceManipulation

##########################################################################
#####The following operations are done on the text before anything else###
##########################################################################

def take_off_chars(in_path_or_str, pattern,  **kwargs):
	"""This functin substitutes a pattern with a given string(\*)"""
	out_path = kwargs.get('out_path', None)
	sub_with = kwargs.get('sub_with', '')
	try:
		p = re.compile(pattern)
		if os.path.exists(in_path_or_str):
			in_f = open(in_path_or_str,'r')
			out_str = ''
			for line in in_f:
				out_str = out_str + p.sub(sub_with, line)

			in_f.close()
		else:
			out_str = p.sub(sub_with, in_path_or_str)

		if out_path:
			out_f = open(out_path, 'w')
			out_f.write(out_str)
			out_f.close()
	except IOError, e:
		print 'cannot open the file', e
	return out_str

##########################################################################
##########The following operations are done on the text after ############
##########################################################################

def r_ffi(ffi_p=''): 
	"""  Returns a ffi list

		Arguments:
		ffi_p -- the path to the file containing ffi

		Outputs:
		ffis_list -- which is the list of ffis, the first two items in the 
				   list is are numpy array and the last is as an int of
				   interaction

	"""
	ffis_list = []
	try:
		ffi_f = open(ffi_p, 'r')
		p = re.compile('[\r\n,\n]') #For the last element (Not needed)
		for line in ffi_f:
			line = p.sub('', line)
			b_line = 	re.split(' ', line)
			ffi = map(lambda a : int(a), b_line)
			ffi = tuple(ffi)
			f_mid_i = (len(ffi) - 1) / 2
			f1 = numpy.array(ffi[:f_mid_i])
			f2 = numpy.array(ffi[f_mid_i:-1])
			ffi_tuple = f1,f2,ffi[-1]
			ffis_list.append(ffi_tuple)
	except IOError, e:
		print "Could not open ffi_p", e,"[r_ffi_for_sec]"
	
	return ffis_list

def dist_kernel(ffi_tuple_1, ffi_tuple_2):
	""" Returns min((|f11-f21|+|f12-f22|), (|f11-f22|+|f12-f21|))"""
	f11 = ffi_tuple_1[0]
	f12 = ffi_tuple_1[1]
	f21 = ffi_tuple_2[0]
	f22 = ffi_tuple_2[1]
	ndist = numpy.linalg.norm
	d1 = ndist(f11-f21) + ndist(f12-f22)
	d2 = ndist(f11-f22) + ndist(f12-f21)
	return min(d1,d2)

def stats(data):
	"""Returns mean and variance of a list given to it"""
	sum = 0.0
	for value in data:
		sum += value
	mean = sum/len(data)
	sum = 0.0
	for value in data:
		sum += (value - mean)**2
	variance = sum/(len(data) - 1)
	return (mean, variance)

def cal_m_v(dist_p=''):
	"""Reads data from a file, and prints the mean and variance of it """
	try:
		dist_f = open(dist_p, 'r')
		ffi_l = []	
		par_ffi_l = []
		
		for line in dist_f:
			b_line = re.split(' ', line)
			ffi_l.append(float(b_line[0]))
			par_ffi_l.append(float(b_line[1]))
		
		(m, v) = stats(ffi_l)
		print "Mean:%f"%m, "Varriance:%f"%v
		(m, v) = stats(par_ffi_l)
		print "Parent Mean:%f"%m, "Parent Varriance:%f"%v

	except IOError, e:
		print "Could not open ffi_p", e,"[r_ffi_for_sec]"

def sep_inter_non(dist_p='', inter_p='', mix_p='', non_p=''):
	"""Reads data from a file and seperates the data and writes that to file
		
		Arguments:
		dist_p -- the path which it is reading from
		inter_p -- path to interacting ones
		mix_p -- path to mix ones
		non_p -- path to non-inetracting ones

	"""
	try:
		dist_f = open(dist_p, 'r')
		non_f = open(non_p, 'w')
		inter_f = open(inter_p, 'w')
		mix_f = open(mix_p, 'w')
		inter_l = []
		non_l = []
		mix_l = []
		for line in dist_f:
			b_line = re.split(' ', line)
			num_line = map(lambda a : float(a), b_line)
			if num_line[2] == 1 and num_line[3] == 1:
				inter_l.append(num_line)
				inter_f.write(line)
			elif num_line[2] == 0 and num_line[3] == 0:
				non_l.append(num_line)
				non_f.write(line)
			elif ((num_line[2] == 1 and num_line[3] == 0) or 
					(num_line[2] == 0 and num_line[3] == 1)):
				mix_l.append(num_line)
				mix_f.write(line)

	except IOError, e:
		print "Could not open file", e,"[sep_inter_non]"

def create_dist_f(ffi_p_p='', ffi_r_p='', par_ffi_p_p='', par_ffi_r_p='', 
			   dist_p=''):
	""" Creates the distance file

		Arguments:
		ffi_p_p -- path to ffi positive or interacting
		ffi_r_p -- path to ffi negative or non-interacting
		par_ffi_p_p -- path to parent(686) positive or interacting
		par_ffi_r_p -- path to parent(686) negative or interacting
		dist_p -- path to the output file 

	"""
	ffi_p = r_ffi(ffi_p_p)
	ffi_r = r_ffi(ffi_r_p)
	par_ffi_p = r_ffi(par_ffi_p_p)
	par_ffi_r = r_ffi(par_ffi_r_p)

	all_ffi = list(ffi_p)
	all_ffi.extend(ffi_r)
	all_par_ffi = list(par_ffi_p)
	all_par_ffi.extend(par_ffi_r)

	max_ffi =  len(all_ffi)
	t1 = time()
	try:
		res_f = open (dist_p, 'w')
		for i in range(max_ffi):
			for j in range(i,max_ffi):
				d_ffi = dist_kernel(all_ffi[i], all_ffi[j])
				d_par_ffi = dist_kernel(all_par_ffi[i], all_par_ffi[j])
				l = (str(d_ffi) + ' ' + str(d_par_ffi) + ' ' + 
					str(all_par_ffi[i][2]) + ' ' + str(all_par_ffi[j][2]) +
					'\n')
				res_f.write(l)
			if i % 50 == 0.0:
				perc_comp = i*100/max_ffi
				print perc_comp, '% :', i, 'out of', max_ffi
	except IOError, e:
		print "File errors", e
	print "Time taken", time() - t1, "secs"


#############################################################################
def cal_perc_p(amino_d):
	ret_amino_d = {}
	total = float(sum(amino_d.values()))
	for a, num in amino_d.items():
		ret_amino_d[a] = num*100/total	
	return ret_amino_d


def perc_p_in_is(ppi_p):
	final_p_amino = {}
	p_amino = {}
	p_num_appeared = {}
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				'yeast_p_sec_struct_dict__formatted.txt'
			)
	seqManip = SequenceManipulation(
			p_seq_p=p_seq_p,
			p_sec_p=p_sec_p,
			ppi_p=ppi_p,
			)
	seqManip.r_ps_dict_from_f()
	pat = re.compile('[\r\n, \n]')
	for p, seq in seqManip.ps_dict.items():
		tmp_aminos = {}
		seq = pat.sub('', seq)
		for i in seq:
			if i in tmp_aminos:
				tmp_aminos[i] += 1
			else:
				tmp_aminos[i] = 0
		p_amino[p] = tmp_aminos	
	seqManip.r_ppi_list_from_f();
	for pseq in seqManip.ppi_list:
		for (k1,k2),v in pseq.items():
			if k1 in p_num_appeared:
				p_num_appeared[k1] += 1
			else:
				p_num_appeared[k1] = 1
			if k2 in p_num_appeared:
				p_num_appeared[k2] += 1
			else:
				p_num_appeared[k2] = 1
	total_p = float(sum(p_num_appeared.values()))
	print total_p
	for p, num in p_num_appeared.items():
		tmp_p = cal_perc_p(p_amino[p])
		for a in tmp_p.keys():
			if a in final_p_amino:
				final_p_amino[a] += num * p_num_appeared[p]
			else:
				final_p_amino[a] = num * p_num_appeared[p]
	return p_amino, p_num_appeared, final_p_amino


if __name__ == '__main__':
	
	name = ('yeast','P1')
	ffi_p_p = os.path.join( os.path.abspath('.'), '_'.join(name) + '_ffi.txt')
	name = ('yeast', 'rN1')
	ffi_r_p = os.path.join( os.path.abspath('.'), '_'.join(name) + '_ffi.txt')

	name = ('yeast','P1')
	par_ffi_p_p = os.path.join(os.path.abspath('.'), '_'.join(name) +
						   '_parent_ffi.txt')
	name = ('yeast', 'rN1')
	par_ffi_r_p = os.path.join(os.path.abspath('.'), '_'.join(name) + 
						   '_parent_ffi.txt')
	#The distance of all and seperation
	#create_dist_f(ffi_p_p, ffi_r_p, par_ffi_p_p, par_ffi_r_p, 'po_dist.txt')
	#sep_inter_non('po_dist.txt', 'po_inter.txt', 'po_mix.txt', 'po_non.txt')
	print '______________________________________________'
	print 'Inter'
	cal_m_v('po_inter.txt')
	print '______________________________________________'
	print 'Mix'
	cal_m_v('po_mix.txt')
	print '______________________________________________'
	print 'Non interacting'
	cal_m_v('po_non.txt')
	print '______________________________________________'
	print 'All data'
	cal_m_v('dist.txt')
	print '______________________________________________'

