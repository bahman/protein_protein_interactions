import re
import os 
import traceback

from p_code_input_output.c_secondary_data_set import secStructManipulation
from p_code_input_output.c_a_ds import AminoSeqManip
from HTML import tag_gen


def gen_amino_gr_html(p_seq_p, p_sec_p, amino_p, binding_sites_p, html_p):
	seqManip = AminoSeqManip(
			p_seq_p=p_seq_p,
			p_sec_p=p_sec_p,
			amino_p=amino_p,
			)
	try:
		binding_sites_f = open(binding_sites_p, 'r')
		html_f = open(html_p, 'w')
		binding_sites_f.readline() # Throw away useless line
		binding_sites_arr = []
		for line in binding_sites_f:
			regexp = re.compile('[\r\n, \n]')
			line = regexp.sub('', line)
			binding_sites_arr.append(line)
			b_line = line.split('\t')
			#Need to add the ppi to the seqManip 
			seqManip.ppi_list.append( {(b_line[0], b_line[1]):'interacting'} )
		seqManip.r_ps_dict_from_f()
		seqManip.f_ssi_list();
		seqManip.f_cci_sub_s_with_c()
		dict_cmp = seqManip.pc_dict 
		html_arr = []
		for i, line in enumerate(binding_sites_arr):
			b_line = line.split('\t')
			p1, p2 = b_line[0], b_line[1]
			p1_b_start, p1_b_end = int(b_line[2]), int(b_line[3])
			p2_b_start, p2_b_end = int(b_line[4]), int(b_line[5])
			p1_b, p2_b = '', ''
			p1_html_seq, p2_html_seq = '', ''
			if p1 in dict_cmp and p2 in dict_cmp:
				if dict_cmp[p1].startswith('NotFound'):
					p1_b_start, p1_b_end = 0, 8
				if dict_cmp[p2].startswith('NotFound'):
					p2_b_start, p2_b_end = 0, 8
				p1_seq, p2_seq = dict_cmp[p1], dict_cmp[p2]
				p1_b, p2_b = p1_seq[p1_b_start:p1_b_end], p2_seq[p2_b_start:p2_b_end]
				p1_html_seq = (p1_seq[:p1_b_start] + '<b>' + p1_b + '</b>'
							+ p1_seq[p1_b_end:])
				p2_html_seq = (p2_seq[:p2_b_start] + '<b>' + p2_b + '</b>'
							+ p2_seq[p1_b_end:])
			elif not p2 in dict_cmp:
				if dict_cmp[p1].startswith('NotFound'):
					p1_b_start, p1_b_end = 0, 8
				p2_html_seq = 'NoShen'
			elif not p1 in dict_cmp:
				if dict_cmp[p2].startswith('NotFound'):
					p1_b_start, p1_b_end = 0, 8
				p1_html_seq = 'NoShen'
			else:
				p1_html_seq, p2_html_seq = 'NoShen', 'NoShen'
			if i%2 == 0:
				html_arr.append('<tr class="even">')
			else:
				html_arr.append('<tr class="odd">')
			html_arr.append('<td>' + p1 + '</td><td>' + p2 + '</td>')
			for ac in seqManip.amino_classes.keys():
				regexp = re.compile(ac)
				match_p1_b = regexp.findall(p1_b)
				match_p1_total = regexp.findall(p1_seq)
				html_arr.append('<td>' + str(len(match_p1_b))+'/'+str(len(match_p1_total)) + '</td>')
				#html_arr.append('<td>' + str(len(match_p1_total)) + '</td>')		
			html_arr.append('<td class="totalS1">' + str(len(p1_b)) + '/' + str(len(p1_seq)) + '</td>')	
			for ac in seqManip.amino_classes.keys():
				regexp = re.compile(ac)
				match_p2_b = regexp.findall(p2_b)
				match_p2_total = regexp.findall(p2_seq)
				html_arr.append('<td>' + str(len(match_p2_b))+'/'+str(len(match_p2_total)) + '</td>')
				#html_arr.append('<td>' + str(len(match_p2_total)) + '</td>')		
			html_arr.append('<td class="totalS2">' + str(len(p2_b)) + '/' + str(len(p2_seq)) + '</td>')	
			html_arr.append('<td>' + p1_html_seq + '</td><td>' + p2_html_seq + '</td>')
			html_arr.append('</tr>')
		css = '''
			<style type="text/css">
			table {
				border-collapse:collapse;
			}
			table, td, th {
				border:1px solid black;
			}
			.even {
				background-color: #FCF6CF;
			}
			.odd{
				background-color: #FEFEF2;
			}
			.totalS1, .totalS2 {
				background-color: #87CBE8;
			}
			</style>
		'''
		html_f.write('<html><head>' + css + '</head><body><table>')
		html_f.write('<th>Protein 1</th><th>Protein 2</th>')
		for ac in seqManip.amino_classes.keys():
			html_f.write('<th>' + ac + ' binding p1' + '</th>')	
			#html_f.write('<th>' + ac + ' total p1' + '</th>')	
		html_f.write('<th>Total s 1</th>')
		for ac in seqManip.amino_classes.keys():
			html_f.write('<th>' + ac + ' binding p2' + '</th>')	
			#html_f.write('<th>' + ac + ' total p2' + '</th>')	
		html_f.write('<th>Total s 2</th>')
		html_f.write('<th>Sequence 1</th><th>Sequence 2</th>')
		for line in html_arr:
			html_f.write(line)
		html_f.write('</table></body></html>')
	except IOError,e:
		#sys.stderr('cannot open the file' + str(e) + '\n')
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
									  sys.exc_info()[2])


def check_in_dict (p_name, p_dict, p_b_start, p_b_end):
		return_tag = ''
		p_found = True
		p_b, p_sec  = '', ''
		if p_name in p_dict:
			p_sec = p_dict[p_name]
			if p_sec.startswith('NotFound'):
				p_b_start, p_b_end , p_found= 0, 8, False
			p_b = p_sec[p_b_start:p_b_end]
			return_tag = p_sec[:p_b_start] 
			if p_found:
				return_tag += tag_gen.span(p_b, 'red')
			else:
				return_tag += p_b
			return_tag += p_sec[p_b_end:]
		else:
			preturn_tag = 'NoShen'
		return return_tag, p_b, p_sec


def count_regexp (regexp_arr, p_selected, p_total, tag_class):
		return_tag = ''
		for regexp in regexp_arr:
			regexp = re.compile(regexp)
			match_p_selected = str(len(regexp.findall(p_selected)))
			match_p_total = str(len(regexp.findall(p_total)))
			return_tag += tag_gen.td(match_p_selected + '/' + match_p_total, '')
		p_selected_len, p_total_len = str(len(p_selected)), str(len(p_total))
		return_tag += tag_gen.td(p_selected_len + '/' + p_total_len, 'totalS1')
		return return_tag


def gen_sec_html(p_sec_p, binding_sites_p, html_p):

	seqManip = secStructManipulation(p_sec_p=p_sec_p)
	seqManip.r_psec_dict_from_f()
	binding_sites_arr = []
	protein_dict = {}

	try:
		html_f = open(html_p, 'w')
		binding_sites_f = open(binding_sites_p, 'r')
		binding_sites_f.readline() # Throw away useless line

		for l in binding_sites_f:
			regexp = re.compile('[\r\n, \n]')
			l = regexp.sub("", l)
			binding_sites_arr.append(l)

		dict_cmp = seqManip.psec_dict 
		table_content = ''

		for i, line in enumerate(binding_sites_arr):

			sec_structs = ['h', 'b', '_']
			b_line = line.split('\t')
			p1, p2 = b_line[0], b_line[1]
			p1_b_start, p1_b_end = int(b_line[2]), int(b_line[3])
			p2_b_start, p2_b_end = int(b_line[4]), int(b_line[5])

			p1_b, p2_b = '', ''
			p1_html_sec, p2_html_sec = '', ''
			p1_found, p2_found = True, True

			p1_html_sec, p1_b, p1_sec = check_in_dict(p1, dict_cmp, p1_b_start,
														p1_b_end)
			p2_html_sec, p2_b, p2_sec = check_in_dict(p2, dict_cmp, p2_b_start,
														p2_b_end)
			tr_str = tag_gen.td(p1, '') + tag_gen.td(p2, '')
			tr_str += count_regexp (sec_structs, p1_b, p1_sec, 'totalS1')
			tr_str += count_regexp (sec_structs, p2_b, p2_sec, 'totalS2')
			tr_str += tag_gen.td(p1_html_sec, '') + tag_gen.td(p2_html_sec, '')
			if i%2 == 0:
				table_content += tag_gen.tr(tr_str, 'even')
			else:
				table_content += tag_gen.tr(tr_str, 'odd')
		th_str = tag_gen.th('Protein 1', '') +tag_gen.th('Protein 2', '')
		for sec_struct in sec_structs:
			th_str += tag_gen.th(sec_struct, '')
		th_str += tag_gen.th('Total P 1', '')
		for sec_struct in sec_structs:
			th_str += tag_gen.th(sec_struct, '')
		th_str += tag_gen.th('Total P 1', '') 
		th_str += tag_gen.th('Secondary Structure 1', '') 
		th_str + tag_gen.th('Secondary Structure 2', '') 
		table_str = tag_gen.table(th_str+table_content, '')
		head = tag_gen.head(tag_gen.common_css)
		body = tag_gen.body(table_str,'')
		html = tag_gen.html( head + body )
		html_f.write(html)
		html_f.close()
	except IOError,e:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
									  sys.exc_info()[2])



if __name__ == '__main__':
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
				#'p_seq_dict_human_binding_sites.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				#'yeast_p_sec_struct_dict__formatted.txt'
				'yeast_p_sec_struct_dict__predicted.txt'
			)
	binding_sites_p = os.path.join(
				os.path.abspath('../data'),
				'binding_sites_265.txt'
				#'binding_sites_h.txt'
			)

	amino_p_l = os.listdir('../data/alphabets')
	for a in amino_p_l:
		amino_p = os.path.join(
					os.path.abspath('../data/alphabets'),
					a
				)
		a = '_'.join(a.split('.')[:2])
		print amino_p
		html_p = os.path.join(
					os.path.abspath('./tmp_html_binding_sites'),
					a
				)
		gen_amino_gr_html(p_seq_p, p_sec_p, amino_p, binding_sites_p, html_p)
	#gen_sec_html(p_sec_p, binding_sites_p, "secondary_structure_predicted.html")
