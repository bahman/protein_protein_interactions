import getopt
import sys
import data_set
import random

from p_code_input_output.c_data_set import SequenceManipulation, c_tar_f 

global input_f, n, verbose

def usage():
	print  """ To be done """

def process_options():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hvi:n:", ["help", "number="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)
	global input_f, n, verbose
	input_f = None
	n = None
	verbose = False
	for o, a in opts:
		if o == "-v":
			verbose = True
		elif o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-i", "--input"):
			input_f = a
		elif o in ("-n", "--number"):
			n = a
		else:
			assert False, "unhandled option"


def rm_p_from_f_write(ppi_p, num):
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				'yeast_p_sec_struct_dict__formatted.txt'
			)
	seqManip = SequenceManipulation(
					p_seq_p=p_seq_p,
					p_sec_p=p_sec_p,
					ppi_p=ppi_p,
			   )
	seqManip.r_ps_dict_from_f()
	seqManip.r_ppi_list_from_f();
	ppi_l = seqManip.ppi_list
	while num != 0:
		random.choice(ppi_l)	
		num -= 1

	seqManip = SequenceManipulation(
					p_seq_p=p_seq_p,
					p_sec_p=p_sec_p,
			   )
	seqManip.r_ps_dict_from_f()
	#seqManip.ppi_list = tmp_ppi_l	
	seqManip.f_ssi_list();
	seqManip.f_cci_sub_s_with_c()
	seqManip.sub_c_with_f()
	seqManip.normalize()
	seqManip.libsvm_format(libsvm_p=p_l[-1]+'.txt') #output is protein name
	c_tar_f(p_l[-1]+'.txt', rm_orig=True)

if __name__ == '__main__':
	process_options()
	print input_f, n, verbose
	data_set.rm_p_from_f_write(input_f, p)
