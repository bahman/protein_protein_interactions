import os
import re
import sys
import traceback
import random
import tarfile

class SequenceManipulation(object):

	def __init__(self, **kwargs):
		self.amino_classes = {
			'1' : '[A | G | V]',
			'2' : '[I | L | F | P]',
			'3' : '[Y | M | T | S]',
			'4' : '[H | N | Q | W]',
			'5' : '[R | K]',
			'6' : '[D | E]',
			'7' : '[C]',
		}

		self.con_tri = {}
		self._reset_con_tri()

		self.ps_dict = {}


		self.ppi_list = []
		self.ssi_list = []
		self.cci_list = []
		self.ffi_list = []
		self.nni_list = []


		#These are the paths which are used in the project
		self.p_seq_p = kwargs.get('p_seq_p', None)
		self.p_sec_p = kwargs.get('p_sec_p', None)
		self.ppi_p = kwargs.get('ppi_p', None)
		self.ssi_p = kwargs.get('ssi_p', None)
		self.cci_p = kwargs.get('cci_p', None)
		self.ffi_p = kwargs.get('ffi_p', None)
		self.nni_p = kwargs.get('nni_p', None)
		self.libsvm_p = kwargs.get('libsvm_p', None)
		self.arff_p = kwargs.get('arff_p', None)
		
	def _reset_con_tri(self):
		"""resets all the values of the con_tri dictionary to zeros"""
		upper_bound = len(self.amino_classes) + 1
		for i in range(1, upper_bound):
			for j in range(1, upper_bound):
				for v in range(1, upper_bound):
					self.con_tri[str(i) + str(j) + str(v)] = 0

	def r_ps_dict_from_f(self,sep='\t', **kwargs):
		"""Creates sequences for the protein names from a given file.
		
			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			p_seq_p -- the path to the file which contains the protein 
					    names or use the one when init the object
		
		"""
		p_seq_p = kwargs.get('p_seq_p', self.p_seq_p)
		if p_seq_p is None:
			err_mes = "The path to p_seq is wrong.[r_ps_dict_from_f]\n"
			sys.stderr.write (err_mes)
			return 0
		try:
			print 'Creating protein to sequence dictionary'
			dict_f = open(p_seq_p, 'r')
			for line in dict_f:
				b_line = line.split(sep)
				p = b_line[0]
				seq = b_line[1]
				self.ps_dict[p] = seq
			print 'Dictionary is made'
			dict_f.close()
		except IOError,e:
			print 'cannot open the file',e

	def r_ppi_list_from_f(self, sep=' ', **kwargs):
		"""Creates a dictionary of pairs of proteins to their interactiosn 
		
			Arguments:
			sep -- The separator used in the file to separate the lines

			Keyword Arguments:
			ppi_p -- The path to the file which contains the protein pairs
				    or it uses the one from when init the object

		
		"""
		ppi_p = kwargs.get('ppi_p',self.ppi_p)
		if ppi_p is None:
			err_mes = "The path to ppi  is wrong.[r_ppi_list_from_f]\n"
			sys.stderr.write (err_mes)
			return 0
		try:
			ppi_f = open(ppi_p, 'r')
			p = re.compile('[\r\n,\n]')
			for line in ppi_f:
				line = p.sub ('', line)
				b_line = line.split(' ')
				b_line = filter(lambda a: a != '', b_line)
				self.ppi_list.append( {(b_line[0], b_line[1]):b_line[2]} )
		except IOError,e:
			#sys.stderr('cannot open the file' + str(e) + '\n')
			traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
		return 1

	def f_ssi_list(self, **kwargs):
		"""  Fills the ssi dictionary and if ssi_p is given to it it will 
			write the dictionary to that path

			Arguments:
			ssi_p -- the path which the dictionary is going to be written
				    into

			Note: The ppi_list should be filled previously

		"""
		if self.ppi_list == []:
			err_mes = "The ppi list need to be filled.[f_ssi_list]\n"
			sys.stderr.write (err_mes)
			return 0
		ssi_p = kwargs.get('ssi_p', self.ssi_p)
		ssi_f = None
		try:
			if ssi_p is not None:
				ssi_f =  open(ssi_p, 'w')
			p = re.compile('[\r\n, \n]')
			#This dict is used for checking what ppis have the same sequence
			tmp_ssi_dict = {}
			for cnt,ppi in enumerate(self.ppi_list):
				for (k1,k2),v in ppi.items():
					s1 = p.sub ('', self.ps_dict[k1])
					s2 = p.sub ('', self.ps_dict[k2])
					val = v
				if s1 == 'NotFound' or s2 == 'NotFound':
					err_mes = (str(cnt) + ' ' + k1 + ' ' + k2 + ' ' + 
							"sequnce for one of them was not found\n")
					sys.stderr.write (err_mes)
					continue
				val = p.sub ('', val)
				# TODO Fix the ones that are duplicates
				# Please note that the 30 sequences are not retrived 
				# properly from the database and they are printed to to err
				if (s1, s2) in tmp_ssi_dict:
					err_mes = (str(cnt) + ' ' + k1 + ' ' + k2 + ' ' + 
							"HAVE THE SAME SEQUENCE DATABASE IS WRONG\n")
					sys.stderr.write (err_mes)
				else:
					if ssi_f is not None:
						ssi_l = s1 + ' ' +  s2 + ' ' + val + '\r\n'
						ssi_f.write(ssi_l)
					self.ssi_list.append( {(s1, s2) : val} )
					tmp_ssi_dict [s1, s2] = val
				
			if ssi_f is not None:
				ssi_f.close()
			del tmp_ssi_dict
		except IOError,e:
			print 'cannot open the file',e

	def _sub_let_with_c(self,string):
		"""  Replaces the amino acids in the string given to it to the 7
			classes of amino acids
		"""
		# Note that subn function returns a tuple and the first element is
		# the substituded string
		string = [string]
		for k,v in self.amino_classes.items():
			tmp_p = re.compile(v)
			string = tmp_p.subn(k,string[0])
		return string[0]
	
	def _sub_i_noni(self,string):
		"""Replaces interactibg and non-interacting with 0 or 1s"""
		# The order is important since if interacting is first, it would be 
		# changed first and then we will be left with non-1
		p_non_inter = re.compile('non-interacting')
		p_inter = re.compile('interacting')
		string = p_non_inter.sub('0', string)
		string = p_inter.sub('1', string)
		return string

	def f_cci_sub_s_with_c(self, sep=' ', **kwargs):
		"""  Substitues all the letters in a given file or ssi list with 
			the amino acid classes, and puts them in the cci list in the
			same order.

			Keyword Arguments:
			ssi_p -- If specified, it uses this file instead of ssi_list
			cci_p -- If specified, the substitued classes are written here

		"""
		ssi_p = kwargs.get('ssi_p', self.ssi_p)
		cci_p = kwargs.get('cci_p', self.cci_p)
		ssi_f = None
		cci_f = None
		try:

			if self.ssi_list != []:
				ssi_s = self.ssi_list
			else:
				if ssi_p is None:
					err_mes = ("The path to ssi_p is wrong." + 
							"[f_cci_sub_s_with_c]\n")
					sys.stderr.write (err_mes)
					return 0
				ssi_f = open(ssi_p, 'r')
				ssi_s = ssi_f
			if cci_p is not None:
				cci_f = open(cci_p,'w') 

			print "Start filling cci list"
			for ssi in ssi_s:
				if type(ssi) == dict:
					for (s1, s2), val in ssi.items():
						ssi = s1 + ' ' + s2 + ' ' + val + '\r\n'
				b_line = re.split(sep, ssi)
				c1 = self._sub_let_with_c(b_line[0])		
				c2 = self._sub_let_with_c(b_line[1])		
				val = self._sub_i_noni(b_line[2])
				cci_line = c1 + ' ' + c2 + ' ' + val 
				if cci_f is not None:
					cci_f.write(cci_line)
				self.cci_list.append({(c1, c2): val})	
			if cci_f is not None:
				cci_f.close()
			if ssi_f is not None:
				ssi_f.close()
			print "Finished filling cci list"

			
		except IOError,e:
			print 'cannot open the file',e

	def _find_freq(self, c_text):
		"""  Returns an array of conjoint_triad which basically shows the 
			number of frequencies """

		matches = re.finditer(r'(?=([1 | 2 | 3 | 4 | 5 | 6 | 7]{3}))',c_text)
		#results = [match.group(1)  for match in matches]

		matches = re.finditer(r'(?=([1 | 2 | 3 | 4 | 5 | 6 | 7]{3}))',c_text)
		results = [match.group(1)  for match in matches]
		self._reset_con_tri()
		for freq in results:
			freq = str(freq)
			self.con_tri[freq] = self.con_tri[freq] + 1
		return self.con_tri

	def _sort_conjoint_triad(self,con_tri):
		keys = con_tri.keys()
		keys.sort()
		return map(con_tri.get, keys)

	def sub_c_with_f(self, sep=' ', **kwargs):
		"""  Substitutes amino classes with frequencies and in the sorted 
			form from 111 to 777

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			ffi_p -- is the path to write the output to	
			cci_p -- is the path to amino acids class file to read 

		"""
		cci_p = kwargs.get('cci_p', self.cci_p)
		ffi_p = kwargs.get('ffi_p', self.ffi_p)
		cci_f = None
		ffi_f = None


		try:
			if self.cci_list != []:
				cci_s = self.cci_list	
			else:
				if cci_p is None:
					err_mes = "The path to cci is wrong.[sub_c_with_f]\n"
					sys.stderr.write (err_mes)
					return 0
				cci_f = open(cci_p, 'r')
				cci_s = cci_f

			if ffi_p is not None:
				ffi_f = open(ffi_p,'w')	
			print 'Start substituding classes with frequencies'
			for line_i, cci in enumerate(cci_s):
				if type(cci) == dict:
					for (c1, c2), val in cci.items():
						cci = c1 + ' ' + c2 + ' ' + val
				b_line = re.split(sep, cci)
				self._reset_con_tri()
				f_1 = self._find_freq(c_text=b_line[0])
				sort_f_1 = self._sort_conjoint_triad(f_1)	

				f_2 = self._find_freq(c_text=b_line[1])
				sort_f_2 = self._sort_conjoint_triad(f_2)	
				# Adding the results of interacting or non-interacting to 
				# the last element 
				sort_f_1 = map(lambda a: str(a), sort_f_1)
				sort_f_2 = map(lambda a: str(a), sort_f_2)
				sort_f_1 = ' '.join(sort_f_1)
				sort_f_2 = ' '.join(sort_f_2)
				if ffi_f is not None:
					ffi_f.write(sort_f_1 + ' ' + sort_f_2 + ' ' +
							  b_line[2])
				self.ffi_list.append( {(sort_f_1, sort_f_2): b_line[2]} )
			print 'Finished substituding classes with frequencies'
			if ffi_f is not None:
				ffi_f.close()
			if cci_f is not None:
				cci_f.close()
		except IOError,e:
			#sys.stderr('cannot open the file' + str(e) + '\n')
			traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
		return 1
	
	def normalize(self, sep= ' ', **kwargs):
		""" This function normalizes the output of frequencies so they would be 
		    between 0 nand 1

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			ffi_p -- is the path frequencies to read
			nni_p -- the out path to normalized frequencies

		"""
		ffi_p = kwargs.get('ffi_p', self.ffi_p)
		nni_p = kwargs.get('nni_p', self.nni_p)
		ffi_f = None
		nni_f = None
		try:
			if nni_p is not None:
				nni_f = open(nni_p, 'w')

			if self.ffi_list != []:
				ffi_s = self.ffi_list
			else:
				if ffi_p is None:
					err_mes = "The path to ffi is wrong.[normalize]\n"
					sys.stderr.write (err_mes)
					return 0
				ffi_f = open(ffi_p, 'r')
				ffi_s = ffi_f

			print '\nstart normalization'
			for ffi in ffi_s:
				if type(ffi) == dict:
					for (f1, f2), val in ffi.items():
						ffi = f1 + ' ' + f2 + ' ' + val
				ffi = ffi.split(' ')

				ffi = map(lambda x: float(x), ffi)
				max_f = max(ffi)
				min_f = min(ffi)

				norm_ffi = map(lambda x: (x - min_f) / (max_f), ffi[:-1])

				norm_ffi.append(ffi[-1])
				str_norm_ffi = map(lambda x : str(x), norm_ffi)
				n_len = len(norm_ffi) / 2
				n1 = str_norm_ffi[:n_len]	
				n2 = str_norm_ffi[n_len:-1]	
				val = str_norm_ffi[-1]
				n1 = ' '.join(n1)
				n2 = ' '.join(n2)
				self.nni_list.append({(n1, n2) : val})
				if nni_f is not None:
					nni_f.write(' '.join(str_norm_ffi) + '\n')

			print 'finished normalization\n'
			if nni_f is not None:
				nni_f.close()

		except IOError,e:
			#sys.stderr('cannot open the file' + str(e) + '\n')
			traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
		return 1

	def libsvm_format(self, **kwargs):
		"""formats a file to an acceptable format for libsvm
		
			Keywords Arguments:
			nni_p -- the path from which the ffis are read from 
			libsvm_p -- the path to write the output to

		"""
		nni_p = kwargs.get('nni_p', self.nni_p)
		libsvm_p = kwargs.get('libsvm_p', self.libsvm_p)
		if libsvm_p is None:
			err_mes = "The path to libsvm is wrong.[libsvm_format]\n"
			sys.stderr.write (err_mes)
			return 0
		nni_f = None
		try:
			if nni_p is not None:
				nni_f = open(nni_p,'r')
				nni_s = nni_f
			else:
				if self.nni_list == []:
					raise(Exception(self.nni_list, "is empty. [libsvm_format]"))
				nni_s = self.nni_list
				
			libsvm_f = open(libsvm_p,'w')
			print "Start changing the format to libsvm"
			for nni in nni_s:
				#TODO Test to see if it works for secondary and non secondary
				if type(nni) is dict:
					if len(nni.keys()[0]) == 2:
						for (n1, n2), v in nni.items():
							nni = n1 + " " + n2 + " " + v
					elif len(nni.keys()[0]) == 4:
						for (n1, sec1, n2, sec2), v in nni.items():
							nni = (n1 + " " + sec1 + " " + n2 + " " + sec2 +
									" " + v)
				freqs = nni.split(" ")
				#l_formatted = freqs[-1]
				i_ni = float(freqs[-1])
				l_formatted =  '+1' if i_ni == 1 else '-1'
				#l_formatted = l_formatted[0]
				for label_num, freq in enumerate(freqs[0:-1]):
					label_num += 1
					if l_formatted != '':
						l_formatted += " "+str(label_num)+":"+str(freq)
					else :
						l_formatted = str(label_num)+":"+str(freq)
				l_formatted += "\n"
				libsvm_f.write(l_formatted)

			print "Finish changing the format to libsvm"
			if nni_f is not None:
				nni_f.close()
			libsvm_f.close()
		except Exception, e:
			#sys.stderr(str(e)+" [libsvm_format]\n")
			traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])

	def arff_format(self, **kwargs):
		"""formats a file to an acceptable format for arff
		
			Keywords Arguments:
			nni_p -- the path from which the ffis are read from 
			arff_p -- the path to write the output to

		"""
		nni_p = kwargs.get('nni_p', self.nni_p)
		arff_p = kwargs.get('arff_p', self.arff_p)
		if arff_p is None:
			err_mes = "The path to arff is wrong.[arff_format]\n"
			sys.stderr.write (err_mes)
			return 0
		try:
			if nni_p is not None:
				nni_f = open(nni_p,'r')
				nni_s = nni_f
				print 'right'
			else:
				nni_s = self.ffi_list
				
			arff_f = open(arff_p,'w')
			print "Start changing the format to libsvm"
			for i, line in enumerate(nni_s):
				freqs = line.split(" ")
				if i == 0:
					l_formatted = '@RELATION ppi \n\n'
					for label_n, freq in enumerate(freqs[0:-1]):
						l_formatted += '@ATTRIBUTE ' + str(label_n) + ' REAL\n'
					l_formatted += '@ATTRIBUTE class {0, 1}\n'
					l_formatted += '\n@DATA\n'
					arff_f.write(l_formatted)
					l_formatted = ''

				l_formatted = '' # init for next loop
				for label_n, freq in enumerate(freqs[0:-1]):
					label_n += 1
					if l_formatted != '':
						l_formatted += ","+str(freq)
					else :
						l_formatted = str(freq)
				l_formatted += ','+freqs[-1][0] #[0] implies ignoring the \n
				l_formatted += "\n"
				arff_f.write(l_formatted)

			print "Finish changing the format"
			nni_f.close()
			arff_f.close()
		except Exception, e:
			traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
			#sys.stderr(str(e)+" [arff_format]\n")


def c_tar_f(f_p, tar_p=None, rm_orig=False):
	try:
		if tar_p is None:
			tar_p = f_p+'.tar.gz'
		tar = tarfile.open(tar_p, "w:gz")
		tar.add(f_p, recursive=False)
		tar.close()
		if rm_orig:
			os.remove(f_p)
	except Exception:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
									  sys.exc_info()[2])


if __name__ == '__main__':
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				'yeast_p_sec_struct_dict__formatted.txt'
			)
	ppi_p =  os.path.join(os.path.abspath('.'), 'y_p1_b1.txt')
	name = ('yeast','_p1_b1_')
	ssi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_ssi.txt'
			)
	cci_p =   os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_cci.txt'
			)
	ffi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_ffi.txt'
			)
	nni_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_nni.txt'
			)
	libsvm_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_libsvm_iff.txt'
			)

	libsvm_p = '/tmp/' + '_'.join(name) + '_libsvm_iff.txt'

	seqManip = SequenceManipulation(
			p_seq_p=p_seq_p,
			p_sec_p=p_sec_p,
			ppi_p=ppi_p,
			#ssi_p=ssi_p,
			#cci_p=cci_p,
			#ffi_p=ffi_p,
			#nni_p=nni_p,
			#libsvm_p = libsvm_p,
			)
	seqManip.r_ps_dict_from_f()
	seqManip.r_ppi_list_from_f();
	seqManip.f_ssi_list();
	seqManip.f_cci_sub_s_with_c()
	seqManip.sub_c_with_f()
	seqManip.normalize()
	seqManip.libsvm_format(libsvm_p = libsvm_p)
	print len(seqManip.ps_dict)
	print len(seqManip.ppi_list)
	print len(seqManip.ssi_list)
	print len(seqManip.cci_list)
	print len(seqManip.ffi_list)
