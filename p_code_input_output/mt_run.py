'''
Created on 6 Aug 2011

@author: bahman
'''
#!/usr/bin/env python

import os
import sys
import traceback
import getpass
import random
from time import sleep
from threading import Thread, RLock
from subprocess import Popen, PIPE
if(sys.hexversion < 0x03000000):
	import Queue
else:
	import queue as Queue

import ssh_connection
import data_set

# svmtrain and gnuplot executable

is_win32 = (sys.platform == 'win32')
if not is_win32:
	svmtrain_exe = "../libsvm-3.1/svm-train"
else:
	# example for windows
	svmtrain_exe = r"..\libsvm-3.1\windows\svm-train.exe"

# global parameters and their default values

fold = 2

c = 128
g = 0.25
ds_paths =[]


global dataset_title, pass_through_string
global out_filename

# experimental

telnet_workers = []
ssh_workers = (
			['zombie'] * 7 +
			['lich'] * 7 +
			['shark'] * 7 +
			['skeleton'] * 7 +
			['ghoul'] * 7 +
			['whale'] * 7 +
			['porpoise'] * 7 +
			['seal'] * 7 +
			['banshee'] * 1 +
			['bass'] * 7 +
			['butterfish'] * 7 +
			['bream'] * 7 +
			['chard'] * 7 +
			['clam'] * 7 +
			['conch'] * 7 +
			['walrus'] * 7 +
			['manatee'] * 7 +
			['mummy'] * 7 +
			['wraith'] * 7 +
			['kelp'] * 7 +
			['octopus'] * 7 +
			['eel'] * 7 +
			['pumpkin'] * 7 +
			['potato'] * 7 +
			['cockle'] * 7 +
			['seacucumber'] * 7 +
			['seapineapple'] * 7 +
			['crab'] * 7 +
			['crayfish'] * 7 +
			['cuttlefish'] * 7 +
			['blowfish'] * 7 +
			['bluefish'] * 7 +
			['celery'] * 7 +
			['pignut'] * 7 +
			['parsnip'] * 7 +
			['swede'] * 7 +
			['sweetpepper'] * 7 +
			['sprout'] * 7 +
			['spinach'] * 7 +
			['brill'] * 7 
			)

nr_local_worker = 8

# process command line options, set global parameters
def process_options(argv=sys.argv):

	global fold
	global c, g
	global ds_paths
	global dataset_title, pass_through_string
	global svmtrain_exe, out_filename
	
	usage = """
		Usage: mt_run.py [-g gamma] [-c cost][-v fold] 
		[-svmtrain pathname] [-out pathname]  
		[additional parameters for svm-train] -f datasets
		"""

	if len(argv) < 2:
		print(usage)
		sys.exit(1)

	pass_through_options = []
	out_filename = None

	are_files = False
	i = 1
	while i < len(argv):
		if are_files:
			ds_paths.append(argv[i])

		elif argv[i] == "-v":
			i = i + 1
			fold = argv[i]
		elif argv[i] == '-c':
			i = i + 1
			c = argv[i]
		elif argv[i] == '-g':
			i = i + 1
			g = argv[i]
		elif argv[i] == '-svmtrain':
			i = i + 1
			svmtrain_exe = argv[i]
		elif argv[i] == '-out':
			i = i + 1
			out_filename = argv[i]
		elif argv[i] == '-f':
			are_files = True
			
		else:
			pass_through_options.append(argv[i])
		i = i + 1


	pass_through_string = " ".join(pass_through_options)
	assert os.path.exists(svmtrain_exe),"svm-train executable not found"	

	out_paths = []
	for p in ds_paths:
		assert os.path.exists(p),"dataset not found"
		ds_title = os.path.split(p)[1]
		out_paths.append(ds_title)
	if out_filename is None:
		out_filename = '{0}.out'.format("_".join(out_paths))


class WorkerStopToken:  # used to notify the worker to stop
		pass

class Worker(Thread):
	def __init__(self,name,job_queue,result_queue):
		Thread.__init__(self)
		self.name = name
		self.job_queue = job_queue
		self.result_queue = result_queue
	def run(self):
		while True:
			ds_path = self.job_queue.get()
			if ds_path is WorkerStopToken:
				###print 'closing ' + self.name
				self.job_queue.put(ds_path)
				# print('worker {0} stop.'.format(self.name))
				break
			try:
				rate = self.run_one(ds_path)
				print self.name + ' Running ' + ds_path
				if rate is None: raise RuntimeError("get no rate")
			except:
				# we failed, let others do that and we just quit
				traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
				self.job_queue.put(ds_path)
				print('worker {0} quit.'.format(self.name))
				break
			else:
				self.result_queue.put((self.name, ds_path,rate))

class LocalWorker(Worker):
	def run_one(self, ds_path):
		###print self.name + ' Running :' + ds_path
		tar_str = 'tar -xf {0} -C /tmp/;'.format(ds_path)
		cmdline = ('{0} -c {1} -g {2} -v {3} {4} /tmp/{5}'.format(svmtrain_exe, 
					c, g, fold, pass_through_string, ds_path[:-7]))
		tar_str += cmdline
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		for line in result.readlines():
			if str(line).find("Cross") != -1:
				###print self.name + ' Finished :' + ds_path
				return float(line.split()[-1][0:-1])

class SSHWorker(Worker):
	def __init__(self,name,job_queue,result_queue,host):
		Worker.__init__(self,name,job_queue,result_queue)
		self.host = host
		self.cwd = os.getcwd()
	def run_one(self, ds_path):
		tar_str = 'tar -xf {0} -C /tmp/'.format(ds_path)
		cmd_str = ('ssh -x {0} ' + 
					'"cd {1};{2};{3} -c {4} -g {5} -v {6} {7} /tmp/{8}"')
		cmdline = cmd_str.format(self.host, self.cwd, tar_str, svmtrain_exe, c,
									g, fold, pass_through_string, ds_path[:-7])
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		for line in result.readlines():
			if str(line).find("Cross") != -1:
				return float(line.split()[-1][0:-1])

class TelnetWorker(Worker):
	def __init__(self,name,job_queue,result_queue,host,username,password):
		Worker.__init__(self,name,job_queue,result_queue)
		self.host = host
		self.username = username
		self.password = password		
	def run(self, ds_path):
		import telnetlib
		self.tn = tn = telnetlib.Telnet(self.host)
		tn.read_until("login: ")
		tn.write(self.username + "\n")
		tn.read_until("Password: ")
		tn.write(self.password + "\n")

		# XXX: how to know whether login is successful?
		tn.read_until(self.username)
		# 
		print('login ok', self.host)
		tn.write("cd "+os.getcwd()+"\n")
		Worker.run(self)
		tn.write("exit\n")			   
	def run_one(self, ds_path):
		cmd_str = '{0} -c {1} -g {2} -v {3} {4} {5}'
		cmdline = cmd_str.format(svmtrain_exe, c, g, fold, pass_through_string,
						ds_path)
		self.tn.write(cmdline+'\n')
		(idx,matchm,output) = self.tn.expect(['Cross.*\n'])
		for line in output.split('\n'):
			if str(line).find("Cross") != -1:
				return float(line.split()[-1][0:-1])

def main():

	global ssh_workers
	# set parameters
	process_options()

	jobs = ds_paths
	# put jobs in queue
	total_jobs = 0
	job_queue = Queue.Queue(0)
	result_queue = Queue.Queue(0)

	for ds_path in jobs:
		total_jobs += 1
		print ds_path
		job_queue.put(ds_path)
	# hack the queue to become a stack --
	# this is important when some thread
	# failed and re-put a job. It we still
	# use FIFO, the job will be put
	# into the end of the queue, and the graph
	# will only be updated in the end
 
	job_queue._put = job_queue.queue.appendleft


	# fire telnet workers
	if telnet_workers:
		nr_telnet_worker = len(telnet_workers)
		username = getpass.getuser()
		password = getpass.getpass()
		for host in telnet_workers:
			TelnetWorker(host,job_queue,result_queue,
					 host,username,password).start()

	#Connect to hosts first
	hostnames = set(ssh_workers) #removing the extras
	hostnames = list(hostnames)

	password = getpass.getpass('Password:')
	if password != '':
		ssh_connector = ssh_connection.SSHConnection(hostnames_l=hostnames,
						password=password)
		# make connection to ssh workers first
		ssh_connector.conn_to_clients_wait()
		ssh_workers = ssh_connector.conn_l
		random.shuffle(ssh_workers)
		# fire ssh workers
		if ssh_workers:
			for host in ssh_workers:
				SSHWorker(host,job_queue,result_queue,host).start()

	# fire local workers
	for i in range(nr_local_worker):
		LocalWorker('local' + str(i), job_queue, result_queue).start()

	# gather results
	done_jobs = {}
	result_f = open(out_filename, 'w')

	for ds_path  in jobs:
		#while ds_path not in done_jobs:
		print '^'*50
		print 'Came for', ds_path
		#NOTE that get waites untile the queue has an item in it
		(worker, ds_path, rate) = result_queue.get()
		print 'Got', ds_path
		print '_'*50
		done_jobs[ds_path] = rate
		cnt_p = data_set.count_lines(ds_path) #Num of proteins
		result_f.write('{0} {1} {2} {3}\n'.format(worker, ds_path, rate, cnt_p))
		result_f.flush()

		nr_done_jobs = len(done_jobs)
		perc = nr_done_jobs * 100.0 / total_jobs
		perc = round(perc, 3)
		state = ("["+ str(nr_done_jobs) + " out of " + str(total_jobs) +
					" " + str(perc)+"%]")
		result = '[{0}] {1} {2} {3}'.format(worker, ds_path, rate, cnt_p)
		print(result + "|" + state)

	job_queue.put(WorkerStopToken)
	if password != '':
			ssh_connector.close_clients()

if __name__ == '__main__':
	#sys.argv.append('../very_small.txt')
	#print sys.argv
	main()
	#process_options()
	#print fold, c, g
	#print ds_paths
