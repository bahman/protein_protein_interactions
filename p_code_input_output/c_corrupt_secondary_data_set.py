import os
import sys
import re
import random

from p_code_input_output.c_data_set import SequenceManipulation
from p_code_input_output.c_only_secondary_data_set import secStructOnlyManipulation

class secStructManipulation(secStructOnlyManipulation):
	
	def __init__(self, **kwargs):
		super(secStructOnlyManipulation, self).__init__(**kwargs)

	def r_psec_dict_from_f(self, sep='\t', **kwargs):
		"""  Fills the p_to_sec_struct_dict with proteins and their secondary
			structure

			Arguments:
			sep -- the separator used in the file
			
			Keyword Arguments:
			p_sec_p -- the path from which the dictionray needs to be read 
					 from

		"""
		p_sec_p = kwargs.get('p_sec_p', self.p_sec_p)
		if p_sec_p is None:
			err_mes = "The path to p_sec is wrong.[r_psec_dict_from_f]\n"
			sys.stderr.write (err_mes)
			return 0

		try:
			print 'Creating p_sec dictionary'
			p_sec_f = open(p_sec_p, 'r')
			for line in p_sec_f:
				b_line = line.split(sep)
				p = b_line[0]
				sec_struct = self._make_line_wrong(b_line[1])
				self.psec_dict[p] = sec_struct
			print 'Dictionary of p_sec is made'
			p_sec_f.close()
		except IOError,e:
			print 'cannot open the file',e
	
	
	def _make_line_wrong(self, line):
		""" Makes 20 precent of the line given wrong and returns it"""
		ret_l = ''
		to_c_l = map(self._rand_cond, line)
		for i, ch in enumerate(line):
			if ch:
				ret_l += self._make_one_wrong(line[i])
			else:
				ret_l += line[i]
		return ret_l

	def _make_one_wrong(self, char):
		""" Returns a different secondary structure compared to char"""
		r_num = random.random()
		if char == 'h':
			if r_num <= 0.5:
				return 'b'	
			else:
				return '_'	
		elif char == 'b':
			if r_num <= 0.5:
				return '_'	
			else:
				return 'h'	
		elif char == '_':
			if r_num <= 0.5:
				return 'h'	
			else:
				return 'b'	
		else:
			raise RuntimeError('The char given to _make_one_wrong is not a sec')
		
	def _rand_cond(self, char):
		if random.random() < 0.2:
			if char == 'h' or char == 'b' or char == '_':
				return True
		else:
			return False


if __name__ == '__main__':
	p_seq_p = os.path.join(
				os.path.abspath('../data'),
				'p_seq_dict_yeast_154828_formatted.txt'
			)
	p_sec_p = os.path.join(
				os.path.abspath('../data'),
				'yeast_p_sec_struct_dict__formatted.txt'
			)
	ppi_p =  os.path.join(os.path.abspath('.'), 'p1_set.txt')
	name = ('yeast','P1')
	# ppi_p =  os.path.join(os.path.abspath('.'), 'r1_set.txt')
	# name = ('yeast','r1')
	ssi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_ssi.txt'
			)
	cci_p =   os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_cci.txt'
			)
	ffi_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_ffi.txt'
			)
	nni_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_nni.txt'
			)
	libsvm_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_libsvm_iff.txt'
			)
	arff_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_arff_iff.txt'
			)

	sec_sec_i_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_sec_sec_i.txt'
			)
	ssi_for_sec_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name)+'_ssi_for_sec.txt'
			)

	ffi_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_ffi.txt'
			)
	nni_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_nni.txt'
			)
	libsvm_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_libsvm_iff.txt'
			)
	arff_parent_p = os.path.join(
				os.path.abspath('.'),
				'_'.join(name) + '_parent_arff_iff.txt'
			)
	seqManip = secStructOnlyManipulation(
				p_seq_p=p_seq_p,
				p_sec_p=p_sec_p,
				ppi_p=ppi_p,
				ssi_p=ssi_p, # Fills writes ssi list to the path
				sec_sec_i_p=sec_sec_i_p,
				ssi_for_sec_p=ssi_for_sec_p,
				cci_p=cci_p,
				ffi_p=ffi_p,
				nni_p=nni_p,
				libsvm_p = libsvm_p,
				arff_p = arff_p,
			)
	seqManip.r_ps_dict_from_f()
	seqManip.r_psec_dict_from_f()
	seqManip.r_ppi_list_from_f();
	seqManip.f_ssi_list();
	#Since it is called before sub of cci the new matrix would be for sec
	seqManip.f_sec_sec_i_list()
	seqManip.f_cci_sub_s_with_c()
	#Note the following is done only so that the same data is not appended to
	#the ffi list at the end of the function (for future use)
	# For 686 (parent) feature
	seqManip.sub_c_with_f(call_parent=True, ffi_p = ffi_parent_p)
	seqManip.normalize(call_parent=True, nni_p = nni_parent_p)
	seqManip.libsvm_format(nni_p = nni_parent_p, libsvm_p=libsvm_parent_p)
	seqManip.arff_format(nni_p = nni_parent_p, arff_p=arff_parent_p)
	# For SEC struct
	seqManip.ffi_list = []
	seqManip.sub_c_with_f()
	seqManip.normalize()
	seqManip.libsvm_format()
	seqManip.arff_format()
	print len(seqManip.ps_dict)
	print len(seqManip.psec_dict)
	print len(seqManip.ppi_list)
	print len(seqManip.ssi_list)
	print len(seqManip.cci_list)
	print len(seqManip.ffi_list)
