import sys
import matplotlib.pyplot as plt

def r_results_sort(res_p):
	num_p_l = []
	acc_l = []
	all_d = {}
	try:
		res_f = open(res_p, 'r')
		for l in res_f:
			l_arr = l.split()
			num_p_l.append(l_arr[-1])
			if l_arr[-1] not in all_d:
				all_d[l_arr[-1]] = l_arr[-2]
			else:
				raise KeyError('num_p already exist' + str(l_arr[-1]))
	except IOError:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
									  sys.exc_info()[2])
	num_p_l.sort()
	acc_l = map(all_d.get, num_p_l)
	return num_p_l, acc_l

if __name__ == '__main__':
	res_p = sys.argv[-1]
	num_p_l, acc_l = r_results_sort(res_p)
	plt.plot(num_p_l, acc_l)
	plt.ylabel('Accuracy')
	plt.xlabel('Number of proteins')
	#plt.show()
	#plt.savefig(res_p+'.png', format='png')
	plt.savefig(res_p+'.png')
