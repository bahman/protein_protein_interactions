common_css =  '''
				<style type="text/css">
				table {
					border-collapse:collapse;
				}
				table, td, th {
					border:1px solid black;
				}
				.even {
					background-color: #FCF6CF;
				}
				.odd{
					background-color: #FEFEF2;
				}
				.totalS1, .totalS2 {
					background-color: #87CBE8;
				}
				.red{
					color: red;
				}
				</style>
			'''


def span(data, classes):
	return '<span class="{0}">{1}</span>'.format(classes, data)


def th(data, classes):
	return '<th class="{0}">{1}</th>'.format(classes, data)


def td(data, classes):
	return '<td class="{0}">{1}</td>'.format(classes, data)


def tr(data, classes):
	return '<tr class="{0}">{1}</tr>'.format(classes, data)


def table(data, classes):
	return '<table class="{0}">{1}</table>'.format(classes, data)


def body(data, classes):
	return '<body class="{0}">{1}</body>'.format(classes, data)


def head(data):
	return '<head>{0}</head>'.format(data)


def html(data):
	return '<html>{0}</html>'.format(data)
