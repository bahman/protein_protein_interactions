import sys
import pickle
import os
from multiprocessing import Pool

import numpy as np
from PyML import *
from PyML import SparseDataSet

def train_test(ds_path):
	td = SparseDataSet(ds_path)
	g, c, fold = 0.25, 128, 5
	score = featsel.FeatureScore('golub')
	fltr = featsel.Filter(score, numFeatures=1024)

	s = SVM()
	m = composite.FeatureSelect(s, fltr)
	r = m.stratifiedCV(td)
	o = open(ds_path+'_8p3t2_golub.pkl', 'wb')
	pickle.dump(r, o)
	o.close();
	print ds_path


if __name__ == '__main__':
	ds_ps = '/tmp/ecnoded_dir/'
	dss = os.listdir(ds_ps)
	dss = [ds_ps+p for p in dss]
	pool = Pool(7)
	pool.map(train_test, dss)

