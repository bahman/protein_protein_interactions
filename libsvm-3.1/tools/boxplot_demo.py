#!/usr/bin/python

#
# Example boxplot code
#

import matplotlib.pyplot as plt


def get_x_col(elem, *args):
	ret_arr = []
	for scheme in args:
		try:
			ret_arr.append(scheme[elem])		
		except IndexError:
			pass
			#ret_arr.append(0)
	return ret_arr

# fake up some data
#spread= rand(50) * 100
#center = ones(25) * 50
#flier_high = rand(10) * 100 + 100
#flier_low = rand(10) * -100
#data =concatenate((spread, center, flier_high, flier_low), 0)
ab = [78.8287, 68.5576, 75.0882, 84.4594, 83.3338, 69.0993, 59]
dssp = [75.88, 77.6912, 78.6873, 82.4611, 83.657]
gbmr = [54.5437,56.9202,71.9011,78.6611,84.3149]
hsdm = [83.4542,60.5917,64.4226,78.6632,84,82.762]
lwi = [83.4542,77.7243,79.3674,78.4315,73.3004,50,60]
lwni = [83.4542,77.7213,78.4526,84.4315,79.1899,63.2761,60]
lzbl = [83.081,50,78.8378,84.4202,83.7822,63.2761]
lzmj = [83.4542,68.9669,75.3137,84.2327,82.735,69.7704]
ml = [79.6112,50,78.4796,83.6528,84,64.9373]
sdm = [83.4542,61.8285,66.9957,78.4345,84]
all_g = [2,3,4,7,10,15,18]
g2 = get_x_col(0, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g3 = get_x_col(1, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g4 = get_x_col(2, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g7 = get_x_col(3, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g10 = get_x_col(4, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g15 = get_x_col(5, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g18 = get_x_col(6, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
data = [g2, g3, g4, g7, g10, g15, g18]
x_axis = ['2','3','4','7','10','18','20']
plt.boxplot(data)
plt.show()

