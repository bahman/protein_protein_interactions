import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


def get_x_col(elem, *args):
	ret_arr = []
	for scheme in args:
		try:
			ret_arr.append(scheme[elem])		
		except IndexError:
			pass
			#ret_arr.append(0)
	return ret_arr

#Box Blot RBF 30k AUC
ab = [0.8593,0.7503,0.8173,0.9098,0.9142,0.9054,0.909,0.8266,0.7359]
dssp = [0.8346,0.8461,0.8588,0.8967,0.902,0.906,0.905]
gbmr = [0.5623,0.5964,0.7824,0.8517,0.872,0.883,0.904]
hsdm = [0.9128,0.6472,0.6964,0.8527,0.87,0.898,0.914,0.908]
lwi = [0.9128,0.8488,0.8603,0.9087,0.907,0.8773,0.85,0.783,0.745]
lwni = [0.9127,0.8488,0.8537,0.9085,0.909,0.883,0.844,0.783,0.745]
lzbl = [0.9087,0.8451,0.8558,0.9077,0.9072,0.908,0.908,0.828]
lzmj = [0.9234,0.7535,0.8193,0.901,0.906,0.899,0.903,0.795]
ml = [0.8675,0.8532,0.9121,0.9177,0.8885]
sdm = [0.9128,0.6632,0.7346,0.8527,0.9032,0.916]
td = [0.8593,0.7503,0.8173,0.9098,0.9142,0.9054,0.909]
lr = [0.9131]
all_g = [2,3,4,7,8,9,10,15,18]
g2 = get_x_col(0, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g3 = get_x_col(1, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g4 = get_x_col(2, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g5 = []
g6 = []
g7 = get_x_col(3, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g8 = get_x_col(4, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g9 = get_x_col(5, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g10 = get_x_col(6, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g11 = []
g12 = []
g13 = []
g14 = []
g15 = get_x_col(7, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g16 = []
g17 = []
g18 = get_x_col(8, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g19 = []
g20 = [0.7507]

g8 = [0.832961,0.829632,0.79014,0.801564,0.842043,0.846992,0.819278,0.844243,0.836968,0.833364,0.844575]
grand8 = [0.822411,0.829494,0.833926,0.83144,0.814682,0.831303,0.822847,0.810372,0.827352,0.774032,0.810097,0.837453,0.829423]
g11 = [
0.817416,0.834237,0.829711,0.767937,0.789112,0.80807,0.799172,0.855222,0.839322
]
grand11 = [ 0.821579645653, 0.878059901029, 0.866652977069, 0.790949094699, 0.80769859468, 0.872467798021, 0.764856551735, 0.853439480612, 0.762920072127, 0.807071956668, 0.774362432968, 0.781820872435, 0.879070324574, 0.875207250499, 0.786898475842
]
grand11 = [
0.775521,0.800958,0.747857,0.847187,0.839649,0.800531,0.802435,0.750432,0.839061,0.837684,0.845395,0.780122,0.75971,0.759446,0.816472
]

data = [g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13, g14, g15, g16, g17, g18, g19, g20]
randomDists = ['2','3','4','5','6','7','8','9','10','11','12','13','14','15', '16','17','18','19','20']
data = [g8, grand8, [0.748066],  g11, grand11, [0.727595]]
randomDists = ['Literature 8', 'Random 8','Selected features 8', 'Literature 11', 'random 11', 'Selected features 11']

# Generate some data from five different probability distributions,
# each with different characteristics. We want to play with how an IID
# bootstrap resample of the data preserves the distributional
# properties of the original sample, and a boxplot is one visual tool
# to make this assessment
'''
numDists = 9
randomDists = ['Normal(1,1)',' Lognormal(1,1)', 'Exp(1)', 'Gumbel(6,4)',
			 	'Normal(1,1)',' Lognormal(1,1)', 'Exp(1)', 'Gumbel(6,4)',
              'Triangular(2,9,11)']
'''
N = 500
norm = np.random.normal(1,1, N)
logn = np.random.lognormal(1,1, N)
expo = np.random.exponential(1, N)
gumb = np.random.gumbel(6, 4, N)
tria = np.random.triangular(2, 9, 11, N)

# Generate some random indices that we'll use to resample the original data
# arrays. For code brevity, just use the same random indices for each array
bootstrapIndices = np.random.random_integers(0, N-1, N)
normBoot = norm[bootstrapIndices]
expoBoot = expo[bootstrapIndices]
gumbBoot = gumb[bootstrapIndices]
lognBoot = logn[bootstrapIndices]
triaBoot = tria[bootstrapIndices]

'''
data = [norm, normBoot,  logn, lognBoot, expo, expoBoot, gumb, gumbBoot,
       tria]
'''

fig = plt.figure(figsize=(10,6))
fig.canvas.set_window_title('A Boxplot Example')
ax1 = fig.add_subplot(111)
plt.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)

bp = plt.boxplot(data, notch=0, sym='+', vert=1, whis=1.5)
#plt.setp(bp['boxes'], color='black')
#plt.setp(bp['whiskers'], color='black')
#plt.setp(bp['fliers'], color='red', marker='+')

'''
# Add a horizontal grid to the plot, but make it very light in color
# so we can use it for reading data values but not be distracting
ax1.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
              alpha=0.5)
'''

# Hide these grid behind plot objects
ax1.set_axisbelow(True)
ax1.set_title('Comparison of Different Amino Acids Groupings')
ax1.set_xlabel('Alphabet size')
ax1.set_ylabel('AUC')

# Now fill the boxes with desired colors
#boxColors = ['red','green','darkkhaki','royalblue']
'''
numBoxes = 9
medians = range(numBoxes)
for i in range(numBoxes):
  box = bp['boxes'][i]
  boxX = []
  boxY = []
  for j in range(5):
      boxX.append(box.get_xdata()[j])
      boxY.append(box.get_ydata()[j])
  boxCoords = zip(boxX,boxY)
  # Alternate between Dark Khaki and Royal Blue
  #k = i % 2
  #boxPolygon = Polygon(boxCoords, facecolor=boxColors[k])
  #ax1.add_patch(boxPolygon)
  # Now draw the median lines back over what we just filled in
  med = bp['medians'][i]
  medianX = []
  medianY = []
  for j in range(2):
      medianX.append(med.get_xdata()[j])
      medianY.append(med.get_ydata()[j])
      plt.plot(medianX, medianY, 'k')
      medians[i] = medianY[0]
  # Finally, overplot the sample averages, with horixzontal alignment
  # in the center of each box
  plt.plot([np.average(med.get_xdata())], [np.average(data[i])],
           color='w', marker='*', markeredgecolor='k')

'''
'''
# Set the axes ranges and axes labels
ax1.set_xlim(0.5, numBoxes+0.5)
top = 40
bottom = -5
ax1.set_ylim(bottom, top)
'''
#xtickNames = plt.setp(ax1, xticklabels=np.repeat(randomDists, 2))
xtickNames = plt.setp(ax1, xticklabels=randomDists)
#plt.setp(xtickNames, rotation=45, fontsize=8)
plt.setp(xtickNames)

'''
# Due to the Y-axis scale being different across samples, it can be
# hard to compare differences in medians across the samples. Add upper
# X-axis tick labels with the sample medians to aid in comparison
# (just use two decimal places of precision)
pos = np.arange(numBoxes)+1
upperLabels = [str(np.round(s, 2)) for s in medians]
weights = ['bold', 'semibold']
for tick,label in zip(range(numBoxes),ax1.get_xticklabels()):
   k = tick % 2
   ax1.text(pos[tick], top-(top*0.05), upperLabels[tick],
        horizontalalignment='center', size='x-small', weight=weights[k],
        color=boxColors[k])
'''

# Finally, add a basic legend
'''
plt.figtext(0.80, 0.08,  str(N) + ' Random Numbers' ,
           backgroundcolor=boxColors[0], color='black', weight='roman',
           size='x-small')
plt.figtext(0.80, 0.045, 'IID Bootstrap Resample',
backgroundcolor=boxColors[1],
           color='white', weight='roman', size='x-small')
plt.figtext(0.80, 0.015, '*', color='white', backgroundcolor='silver',
           weight='roman', size='medium')
plt.figtext(0.815, 0.013, ' Average Value', color='black', weight='roman',
           size='x-small')
'''
plt.grid(True)
plt.show()
