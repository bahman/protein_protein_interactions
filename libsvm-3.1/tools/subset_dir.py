import os
import subprocess
import argparse
from multiprocessing import Pool


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Creats training and testing subsets')
	parser.add_argument('directory', metavar='dir', type=str,
	                   help='The directory where all files to be divided are')
	parser.add_argument('size', metavar='training size', type=int,
	                   help='This is the size of the training set')
	args = parser.parse_args()
	ps = os.listdir(args.directory)
	ps = [p for p in ps if not p.endswith('.pkl')]
	cmds = ['python subset.py {0} {1} {0}.train {0}.test'.format(
		args.directory+p, args.size) for p in ps]

	for c in cmds:
		subprocess.Popen(c, shell=True)
