This project is for studying PPIs. We predict unknown interactions based on the known 
interacting protein pairs. So the main point which is studied in this project is 
grouping of 20 amino acids in order to reduce the dimensionality of the problem at hand.

The code has the following folders:
	data 
		-- alphabets
	download_p_seq_dict
	HTML
	libsvm-3.1
	p_code_input_output
	raw_data_manip
	tools

data: includes the files which were used for training.
	-- alphabets: Has different encoding schemes of amino acids
download_p_seq_dict: Includes the files which were used to download protein sequences (amino
	acids)
HTML: It was used for presenting binding sites in an html page
libsvm-3.1: Used for training on the data
p_code_input_output: This includes files for encoding the data, so mainly used before training
raw_data_manip: this would include files which are used to manipulate the raw data
tools: used in for other purposes such as plotting


So the IMPORTANT file for you to get started on PPI problem is:
p_code_input_output/c_a_ds.py

This file is used for encoding the ppi training and testing files. This provides the samples in
a libsvm format of:
output 1:first_attr 2:second_attr ....

The function c_ds, and the last few lines of the code are the parts which are needed to be
changed.

so

if __name__ == '__main__':
	out_d = 'ecnoded_dir'
	in_p =  '../data/y_p1_r1.txt'
	out_name = ('yeast','p1_r1')
	amino_p_l = os.listdir('../data/alphabets')
	amino_p_l = [l for l in amino_p_l if l.startswith('ab.2.')]
	c_ds_args = [(l, in_p, out_d, out_name) for l in amino_p_l]
	pool = Pool(15)
	pool.map(c_ds, c_ds_args)

The out_dir is the path to the directory which all of the encoded would go to, the in_p is the
protein pairs which are used for training or testing. and out_name are the names which will be 
appended to the files cerated. 
amino_p_l = [l for l in amino_p_l if l.startswith('ab.2.')] is used for filtering out the 
alphabets which are used to encode the data.
Pool will depend on the number of processor on your computer.
